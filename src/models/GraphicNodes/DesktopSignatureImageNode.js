// @flow
import SignatureImageNode from './SignatureImageNode'

export default class DesktopSignatureImageNode extends SignatureImageNode {
  _startMaxWidth: number = 200
  _startMaxHeight: number = 200
}
