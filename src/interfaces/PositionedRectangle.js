// @flow
export interface PositionedRectangle {
  left: number;
  top: number;
  width: number;
  height: number
}
