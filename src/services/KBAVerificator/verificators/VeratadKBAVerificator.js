import axios from 'axios'
import UnknownPersonKbaError from '../../../errors/UnknownPersonKbaError'

export default class VeratadKBAVerificator {
  constructor ({ storage }) {
    this.apiBaseUrl = process.env.VUE_APP_API_BASE_URL
    this.storage = storage
  }

  setCredentials (credentials) {
    this.credentials = credentials

    return this
  }

  async generateQuestions (idData) {
    const requestUrl = `${this.apiBaseUrl}/questions/generate`
    const params = { headers: this.credentials }
    const { data } = await axios.post(requestUrl, idData, params)

    if (data && data.result && data.result.action === 'FAIL') {
      if (data.result.detail === 'NO MATCH') {
        throw new UnknownPersonKbaError('Couldn\'t find a person with provided details in database')
      }

      throw new Error(data.result.detail)
    }

    if (
      !data || !data.output || !data.output.questions || !data.output.questions.questions ||
      Object.keys(data.output.questions.questions).length === 0
    ) {
      throw new Error('No questions returned')
    }

    this.storage.setCompleteData(data)
      .removeAnswers()

    return data
  }

  getQuestions () {
    const questions = this.storage.getQuestions()

    if (!questions) {
      return null
    }

    return questions.map(({ id, prompt, type, answers }) => ({
      id,
      type,
      text: prompt,
      options: answers.map(({ text }) => ({
        name: text,
        value: text
      }))
    }))
  }

  saveAnswer ({ answer, questionId }) {
    const answers = this.storage.getAnswers()

    answers[questionId] = [answer]
    this.storage.setAnswers(answers)

    return this
  }

  async submitAnswers () {
    const answers = this.storage.getAnswers()
    const requestUrl = `${this.apiBaseUrl}/questions/answer`
    const params = { headers: this.credentials }
    const completeData = this.storage.getCompleteData()
    const { continuations: { questions: { template: { token } } } } = completeData

    try {
      const { data } = await axios.post(requestUrl, { answers, token }, params)

      return (!data || !data.result || !data.result || data.result.issues.indexOf('QUESTION CHECK FAILED') === -1)
    } catch (e) {
      return false
    }
  }
}
