import { connect, LocalVideoTrack } from 'twilio-video'
import { insertOneChild, getAudioContainerHandler } from '../../services/utils'
import { sendLogs } from '../../plugins/requestsLogging'
import { NotarySession } from '../../services/NotarySession'

let roomName = process.env.VUE_APP_ROOM_NAME
let activeRoom
let previewTracks
let globalOptions

const getAudioContainer = getAudioContainerHandler()

// Attach the Tracks to the DOM.
const attachTracks = (tracks, container, options = {}) => {
  const { isRemote } = options
  const audioContainer = getAudioContainer()

  if (isRemote) {
    const remoteAudioTrack = tracks.find(track => track.kind === 'audio')

    if (remoteAudioTrack && remoteAudioTrack.mediaStreamTrack) {
      window.remoteAudioStream = new MediaStream([remoteAudioTrack.mediaStreamTrack])
    }
  }

  // Remove audio track
  if (globalOptions && globalOptions.isAudioEnabled === false) {
    tracks = tracks.filter(track => track.kind !== 'audio')
  }

  tracks.forEach((track) => {
    let targetContainer = track.kind === 'audio' ? audioContainer : container

    if (targetContainer) {
      return insertOneChild(track.attach(), targetContainer)
    }
  })
}

// Attach the Participant's Tracks to the DOM.
const attachParticipantTracks = (participant, container, options = {}) => {
  const tracks = Array.from(participant.tracks.values())

  attachTracks(tracks, container, options)
}

// Detach the Tracks from the DOM.
const detachTracks = tracks => (
  tracks.forEach(track => (
    track.detach()
      .forEach(detachedElement => detachedElement.remove())
  ))
)

// Detach the Participant's Tracks from the DOM.
const detachParticipantTracks = (participant) => {
  const tracks = Array.from(participant.tracks.values())

  return detachTracks(tracks)
}

const initMeeting = (name, options) => {
  globalOptions = options

  if (globalOptions && globalOptions.sessionId) {
    roomName = `notary-session-${globalOptions.sessionId}`
  }

  if (globalOptions && globalOptions.roomName) {
    roomName = globalOptions.roomName
  }

  return new Promise((resolve, reject) => {
    if (globalOptions && globalOptions.isMeetingDisabled) {
      return resolve()
    }

    if (window.room) {
      roomJoined(window.room)

      return resolve()
    }

    // Obtain a token from the server in order to connect to the Room.
    $.ajax(`${process.env.VUE_APP_API_BASE_URL}/twilio/token`).success((data) => {
      if (!roomName) {
        alert('Please enter a room name.')
        sendLogs({
          sessionId: NotarySession.init().getSessionId(),
          component: 'Twilio',
          message: 'No room name entered',
          level: 'error',
          service: 'APP',
          userAgent: window.navigator.userAgent,
          extra: {
            currentUrl: window.location.href
          }
        })

        return reject(new Error('Please enter a room name.'))
      }

      sendLogs({
        sessionId: NotarySession.init().getSessionId(),
        component: 'Twilio',
        message: 'joining the room',
        level: 'debug',
        service: 'APP',
        userAgent: window.navigator.userAgent,
        extra: {
          currentUrl: window.location.href
        }
      })

      const connectOptions = {
        name: roomName,
        video: { width: 480 },
        audio: true
      }

      if (previewTracks) {
        connectOptions.tracks = previewTracks
      }

      // Join the Room with the token from the server and the
      // LocalParticipant's Tracks.
      connect(data.token, connectOptions).then(
        (room) => {
          roomJoined(room)
          sendLogs({
            sessionId: NotarySession.init().getSessionId(),
            component: 'Twilio',
            message: 'Joined to room',
            level: 'debug',
            service: 'APP',
            userAgent: window.navigator.userAgent,
            extra: {
              currentUrl: window.location.href
            }
          })
          resolve()
        },
        (error) => {
          sendLogs({
            sessionId: NotarySession.init().getSessionId(),
            component: 'Twilio',
            message: 'Coudn\'t connect to Twilio',
            level: 'error',
            service: 'APP',
            userAgent: window.navigator.userAgent,
            extra: {
              error: {
                ...error,
                message: error.message,
                name: error.name
              },
              currentUrl: window.location.href
            }
          })
        }
      )
    })
  })
}

const roomJoined = (room) => {
  window.room = activeRoom = room

  // Attach LocalParticipant's Tracks, if not already attached.
  const localPreviewContainer = document.getElementById('local-media')
  if (localPreviewContainer !== null && !localPreviewContainer.querySelector('video')) {
    attachParticipantTracks(room.localParticipant, localPreviewContainer)

    if (globalOptions && globalOptions.onLocalUserJoined) {
      globalOptions.onLocalUserJoined(room.localParticipant)
    }
  }

  const remotePreviewContainer = document.getElementById('remote-media')
  if (room.participants.size && remotePreviewContainer !== null) {
    const participant = room.participants.values().next().value // Only one participant allowed

    attachParticipantTracks(participant, remotePreviewContainer, { isRemote: true })

    if (globalOptions && globalOptions.onParticipantJoined) {
      globalOptions.onParticipantJoined(participant)
    }
  }

  // When a Participant joins the Room, log the event.
  room.on('participantConnected', (participant) => {
    if (globalOptions && globalOptions.onParticipantJoined) {
      globalOptions.onParticipantJoined(participant)
    }
  })

  // When a Participant adds a Track, attach it to the DOM.
  room.on('trackAdded', (track) => {
    const previewContainer = document.getElementById('remote-media')

    attachTracks([track], previewContainer, { isRemote: true })
  })

  // When a Participant removes a Track, detach it from the DOM.
  room.on('trackRemoved', (track) => {
    detachTracks([track])
  })

  // When a Participant leaves the Room, detach its Tracks.
  room.on('participantDisconnected', (participant) => {
    detachParticipantTracks(participant)

    if (globalOptions && globalOptions.onParticipantDisconnected) {
      globalOptions.onParticipantDisconnected(participant)
    }
  })

  // Once the LocalParticipant leaves the room, detach the Tracks
  // of all Participants, including that of the LocalParticipant.
  room.on('disconnected', () => {
    if (previewTracks) {
      previewTracks.forEach((track) => {
        track.stop()
      })
      previewTracks = null
    }

    detachParticipantTracks(room.localParticipant)
    room.participants.forEach(detachParticipantTracks)
    activeRoom = null

    if (globalOptions && globalOptions.onLocalUserDisconnected) {
      globalOptions.onLocalUserDisconnected(room.localParticipant)
    }
  })
}

const leaveRoomIfJoined = () => {
  if (activeRoom) {
    activeRoom.disconnect()
  }
}

const leaveMeeting = () => {
  if (window.room) {
    window.room.disconnect()
    window.room = null
  }

  return true
}

let screenCaptureTrack = null

const startRecording = async () => {
  const stream = await navigator.mediaDevices.getDisplayMedia()

  screenCaptureTrack = stream.getTracks()[0]

  const screenTrack = new LocalVideoTrack(screenCaptureTrack, { name: 'screenShare' })

  if (!window.room) {
    throw new Error('Room is not yet created')
  }

  await window.room.localParticipant.publishTrack(screenTrack)
}

const stopRecording = async () => {
  if (screenCaptureTrack) {
    screenCaptureTrack.stop()
  }

  return true
}

// When we are about to transition away from this page, disconnect
// from the room, if joined.
window.addEventListener('beforeunload', leaveRoomIfJoined)

export {
  initMeeting,
  leaveMeeting,
  startRecording,
  stopRecording
}
