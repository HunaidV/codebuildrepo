let instance = null

export default class Logger {
  static create () {
    if (!instance) {
      instance = new Logger()
    }

    return instance
  }

  log (msg) {
    if (process.env.NODE_ENV === 'production') {
      return null
    }

    console.log(msg)
  }

  debug (msg) {
    if (process.env.NODE_ENV === 'production') {
      return null
    }

    console.debug(msg)
  }
}
