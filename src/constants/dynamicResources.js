// @flow
import { AUTO_CAPTURE } from './idVerificators'
import { ROLE_MOBILE_VERIFICATION_USER, ROLE_OFFICIAL, ROLE_USER } from './roles'
import IdVerificatorFactory from '../services/IdVerificators/IdVerificatorFactory'

export const DYNAMIC_RESOURCES = {
  [ROLE_USER]: () => {
    const idVerificator = IdVerificatorFactory.create(AUTO_CAPTURE)

    return idVerificator.loadSrc()
  },
  [ROLE_MOBILE_VERIFICATION_USER]: () => {
    const idVerificator = IdVerificatorFactory.create(AUTO_CAPTURE)

    return idVerificator.loadSrc()
  },
  [ROLE_OFFICIAL]: [
    '//cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js',
    '//cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.worker.min.js'
  ]
}
