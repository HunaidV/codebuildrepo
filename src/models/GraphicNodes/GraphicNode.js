// @flow
export default class GraphicNode {
  id: string|null = null
  isFixed: boolean = false
  isEditable: boolean = false
  pageNumber: number = 1

  setPageNumber (pageNumber: number): this {
    this.pageNumber = pageNumber

    return this
  }

  setFixed (): this {
    this.isFixed = true

    return this
  }
}
