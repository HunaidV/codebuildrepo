const handlerKey = '__documentClickDirectiveHandler'

export default {
  bind: (el, binding) => {
    el[handlerKey] = binding.value
    document.addEventListener('click', el[handlerKey])
  },
  unbind: (el) => {
    document.removeEventListener('click', el[handlerKey])
    el[handlerKey] = null
  }
}
