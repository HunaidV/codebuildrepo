// @flow
import { KBA_FLOW, DEFAULT_FLOW } from './flows'

export const NO_SUB_STAGE = 'NO_SUB_STAGE'
export const SET_RECORDING_CLICKED = 'SET_RECORDING_CLICKED'
export const KBA_RULES_ACCEPTED = 'KBA_RULES_ACCEPTED'
export const KBA_USER_DATA_CONFIRMED = 'KBA_USER_DATA_CONFIRMED'
export const KBA_QUIZ_START_ALLOWED = 'KBA_QUIZ_START_ALLOWED'

export const ORDERED = {
  [DEFAULT_FLOW]: [
    NO_SUB_STAGE
  ],
  [KBA_FLOW]: [
    NO_SUB_STAGE,
    KBA_RULES_ACCEPTED,
    KBA_USER_DATA_CONFIRMED,
    KBA_QUIZ_START_ALLOWED,
    SET_RECORDING_CLICKED
  ]
}

export const getSubStageIndex = (subStage: string, flow: string = KBA_FLOW): number => {
  let subStages = ORDERED[flow]

  if (!subStages) {
    subStages = ORDERED[DEFAULT_FLOW]
  }

  return subStages.findIndex(curr => curr === subStage)
}

export const getNextSubStage = (curr: string, flow: string = KBA_FLOW): string => {
  const index = getSubStageIndex(curr, flow)

  let subStages = ORDERED[flow]

  if (!subStages) {
    subStages = ORDERED[DEFAULT_FLOW]
  }

  if (index === -1 || index >= ORDERED[flow].length - 1) {
    return subStages[0]
  }

  return subStages[index + 1]
}
