import { NotarySession } from '../services/NotarySession'

const logError = async (error = {}) => {
  try {
    const sessionId = NotarySession.init().getSessionId()
    const message = (error.reason && error.reason.message) || error.message || null
    const stack = (error.reason && error.reason.stack) || error.stack || null
    const data = {
      sessionId,
      message,
      stack,
      level: 'error',
      service: 'APP',
      userAgent: window.navigator.userAgent
    }

    if (error.extra) {
      data.extra = error.extra
    }

    await navigator.sendBeacon(`${process.env.VUE_APP_API_BASE_URL}/logs`, JSON.stringify(data))
  } catch (e) {
    console.error(e)
  }
}

window.onerror = (message, source, lineno, colno, error) => {
  logError(error)
  console.error(message)
}

window.addEventListener('unhandledrejection', (event) => {
  logError(event)
  console.error(event)
})
