// @flow
export interface InternalCommand {
  command: string;
  timestamp: Date;
}
