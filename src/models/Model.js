// @flow
import { pad } from '../services/utils'
import { utils } from 'shared'

interface FormElement {
  type: string;
  property: string;
  label: string;
}

export default class Model {
  formElements: Array<FormElement>
  _originalFormElements: Array<FormElement>

  normalizeDate (date: Date = new Date()): string {
    const yyyy = date.getFullYear()
    const MM = pad(date.getMonth() + 1)
    const dd = pad(date.getDate())
    const hh = pad(date.getHours())
    const mm = pad(date.getMinutes())

    return `${yyyy}-${MM}-${dd}T${hh}:${mm}`
  }

  setFormElements (formElements: Array<FormElement>): void {
    this.formElements = formElements
    this._originalFormElements = utils.clone(formElements)
  }

  getFormElements (): Array<FormElement> {
    return this.formElements
  }

  removeFormElements (...keys: Array<string>): this {
    let i = this.formElements.length

    while (i--) {
      for (let j = 0; j < keys.length; j++) {
        const key = keys[j]

        if (this.formElements[i].property === key) {
          this.formElements.splice(i, 1)
          i--
        }
      }
    }

    return this
  }

  restoreFormElements (...keys: Array<string>): this {
    const activeFormElements = this.formElements.map(({ property }) => property)
    const neededElements = this._originalFormElements
      .filter(({ property }) => (
        keys.indexOf(property) >= 0 && activeFormElements.indexOf(property) === -1
      ))

    this.formElements = this.formElements.concat(neededElements)

    return this
  }

  setProp (key: string, val: any): this {
    // $FlowFixMe
    this[key] = val

    return this
  }

  populate (input: Object = {}): this {
    if (input instanceof Object === false) {
      return this
    }

    const keys = Object.keys(input)

    for (let i = 0; i < keys.length; i++) {
      if (this.hasOwnProperty(keys[i])) {
        this.setProp(keys[i], input[keys[i]])
      }
    }

    return this
  }
}
