// @flow
import { NotarySession } from '../services/NotarySession'

export default {
  computed: {
    isKBAFlow (): boolean {
      return this.getSession().isKBAFlow()
    },
    isDTMFlow (): boolean {
      return this.getSession().isMobileVerificationFlow()
    },
    isPKFlow (): boolean {
      return !(this.isDTMFlow || this.isKBAFlow)
    }
  },
  methods: {
    getSession (): NotarySession {
      if (!this.$options.session) {
        this.$options.session = NotarySession.init()
      }

      return this.$options.session
    }
  }
}
