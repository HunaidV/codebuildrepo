// @flow
import { USER_SIGNED } from '../constants/stages'
import TextNode from '../models/GraphicNodes/TextNode'
import ImageNode from '../models/GraphicNodes/ImageNode'

interface NodesMixinData {
  addedImages: Array<ImageNode>,
  textNodes: Array<TextNode>,
}

export default {
  data: (): NodesMixinData => ({
    addedImages: [],
    textNodes: []
  }),
  watch: {
    stage (stage: string): void {
      if (stage !== USER_SIGNED) {
        this.clearNodes()
      }
    }
  },
  methods: {
    clearNodes (): void {
      this.addedImages = []
      this.textNodes = []
    }
  }
}
