// @flow

import { mapState } from 'vuex'
import { USER_SIGNING_PROCESS_START } from '../constants/stages'

export default {
  data: () => ({
    signatureBoxPointerShown: false
  }),
  computed: {
    ...mapState(['stage']),
    signatureBoxPointerAvailable (): boolean {
      return this.stage === USER_SIGNING_PROCESS_START
    }
  },
  methods: {
    toggleSignatureBoxPointer (): void {
      this.signatureBoxPointerShown = !this.signatureBoxPointerShown
    }
  }
}
