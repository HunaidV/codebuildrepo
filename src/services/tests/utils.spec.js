import {
  toInt, pad, randomUserName,
  parseQueryParameters, getCurrentDocument, isLastDocumentUnprocessed,
  joinIfNotEmpty
} from '../utils'

describe('utils', () => {
  describe('toInt', () => {
    it('Should return the same number if number passed', () => {
      expect(toInt(1)).toBe(1)
    })

    it('Should return 0 for "0"', () => {
      expect(toInt('0')).toBe(0)
    })

    it('Should return 0 for invalid string', () => {
      expect(toInt('Some invalid string')).toBe(0)
    })

    it('Should return negative number', () => {
      expect(toInt('-1')).toBe(-1)
    })

    it('Should floor decimal from string', () => {
      expect(toInt('1.8')).toBe(1)
    })

    it('Should floor decimal from number', () => {
      expect(toInt(1.833)).toBe(1)
    })

    it('Should return infinity', () => {
      expect(toInt(Infinity)).toBe(Infinity)
    })
  })

  describe('pad', () => {
    it('Should add leading zero without optional parameters if one digit is passed', () => {
      expect(pad(1)).toBe('01')
    })

    it('Should add 3 leading zeros with optional parameter', () => {
      expect(pad(1, 4)).toBe('0001')
    })

    it('Should add 5 leading zeros with optional parameter', () => {
      expect(pad(12, 7)).toBe('0000012')
    })

    it('Should work with number-like strings', () => {
      expect(pad('12', 7)).toBe('0000012')
    })

    it('Should work with any strings', () => {
      expect(pad('str', 7)).toBe('0000str')
    })
  })

  describe('randomUserName', () => {
    it('Should generate random unique number', () => {
      const results = []

      for (let i = 0; i < 10000; i++) {
        results.push(randomUserName())
      }

      const duplicates = results.find((element, index) => results.indexOf(element, index + 1) > -1)

      expect(duplicates).toBe(undefined)
    })
  })

  describe('parseQueryParameters', () => {
    it('Should parse shallow query string with "?" on the start', () => {
      const query = '?sid=123&token=432'
      const map = parseQueryParameters(query)

      expect(map instanceof Map).toBe(true)
      expect(map.get('sid')).toBe('123')
      expect(map.get('token')).toBe('432')
      expect(map.get('invalid')).toBe(undefined)
    })

    it('Should parse shallow query string without "?" on the start', () => {
      const query = 'sid=sid-id-here&token=token-here'
      const map = parseQueryParameters(query)

      expect(map instanceof Map).toBe(true)
      expect(map.get('sid')).toBe('sid-id-here')
      expect(map.get('token')).toBe('token-here')
      expect(map.get('invalid')).toBe(undefined)
    })
  })

  describe('getCurrentDocument', () => {
    it('Should return first unprocessed document', () => {
      const docs = [
        { id: 0, status: 'NOTARIZED' },
        { id: 1 },
        { id: 2, status: 'SKIPPED' },
        { id: 3 }
      ]
      const res = getCurrentDocument(docs)

      expect(typeof res).toBe('object')
      expect(res).toHaveProperty('id')
      expect(res.id).toBe(1)
    })

    it('Should return null if all documents are processed', () => {
      const docs = [
        { id: 0, status: 'NOTARIZED' },
        { id: 1, status: 'NOTARIZED' },
        { id: 2, status: 'SKIPPED' },
        { id: 3, status: 'NOTARIZED' }
      ]
      const res = getCurrentDocument(docs)

      expect(res).toBe(null)
    })

    it('Should return the first document if it\'s one doc session event the doc is processed', () => {
      const docs = [{ id: 0, status: 'NOTARIZED' }]
      const res = getCurrentDocument(docs)

      expect(typeof res).toBe('object')
      expect(res).toHaveProperty('id')
      expect(res.id).toBe(0)
    })
  })

  describe('isLastDocumentUnprocessed', () => {
    it('Should return true if there is only one unprocessed document left', () => {
      const docs = [
        { id: 0, status: 'NOTARIZED' },
        { id: 1, status: 'NOTARIZED' },
        { id: 2, status: 'SKIPPED' },
        { id: 3 }
      ]
      const res = isLastDocumentUnprocessed(docs)

      expect(res).toBe(true)
    })

    it('Should return false if there are no unprocessed document left', () => {
      const docs = [
        { id: 0, status: 'NOTARIZED' },
        { id: 1, status: 'NOTARIZED' },
        { id: 2, status: 'SKIPPED' },
        { id: 3, status: 'SKIPPED' }
      ]
      const res = isLastDocumentUnprocessed(docs)

      expect(res).toBe(false)
    })

    it('Should return true for one documents session', () => {
      const docs = [
        { id: 0, status: 'NOTARIZED' }
      ]
      const res = isLastDocumentUnprocessed(docs)

      expect(res).toBe(true)
    })
  })

  describe('joinIfNotEmpty', () => {
    it('Should filter out nullable entities', () => {
      const res = joinIfNotEmpty(null, undefined, 'Some string', 0, 'test')

      expect(res).toBe('Some string test')
    })

    it('Should return empty string for nullable arguments', () => {
      const res = joinIfNotEmpty(null, undefined, 0, false)

      expect(res).toBe('')
    })
  })
})
