// @flow
export default {
  data: () => ({
    errorMessage: null
  }),
  methods: {
    setError (e: Error|string|Object): void {
      console.error(e)
      this.errorMessage = e.message || e
    }
  }
}
