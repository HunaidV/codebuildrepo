// @flow
import type { Rectangle } from './Rectangle'

export interface CanvasMeta {
  canvasContext: CanvasRenderingContext2D;
  viewport: Rectangle;
}
