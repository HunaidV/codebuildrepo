import Vue from 'vue'
import Router from 'vue-router'

import Typography from './../views/Typography.vue'
import Dashboard from './../views/Dashboard'
import RouteGuard from './../services/RouteGuard/RouteGuard'
import TestCall from './../views/TestCall'
import Forbidden from './../views/Forbidden'
import userRoutes from './userRoutes'
import officialRoutes from './officialRoutes'
import mobileVerificationRoutes from './mobileVerificationRoutes'
import saveUserAgent from './../services/routeHooks/saveUserAgent'

Vue.use(Router)

const router = new Router({
  routes: [
    ...userRoutes,
    ...officialRoutes,
    ...mobileVerificationRoutes,
    {
      path: '/typography',
      name: 'typography',
      component: Typography,
      meta: {
        serviceRoute: true
      }
    }, {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        serviceRoute: true
      }
    }, {
      path: '/test',
      name: 'testCall',
      component: TestCall,
      meta: {
        serviceRoute: true
      }
    }, {
      path: '/forbidden',
      name: 'forbidden',
      component: Forbidden,
      meta: {
        serviceRoute: true
      }
    }
  ]
})

RouteGuard.registerRouter(router)
RouteGuard.addAfterAuthHook(saveUserAgent)

export default router
