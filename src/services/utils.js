// @flow
import { AUDIO_TRACKS_CONTAINER_ID } from '../constants/elements'
import {
  MONTHS, DATE_FORMAT_FULL, DATE_FORMAT_MONTH_AND_DAY,
  DATE_FORMAT_DAY, DATE_FORMAT_MONTH, DATE_FORMAT_YEAR
} from '../constants/dates'
import { PROCESSED_STATUSES } from '../constants/documents'
import type { Rectangle } from '../interfaces/Rectangle'
import type { PdfPage } from '../interfaces/PdfPage'
import type { NotaryDocument } from '../interfaces/NotaryDocument'
import { ROLE_MOBILE_VERIFICATION_USER, ROLE_OFFICIAL, ROLE_USER } from '../constants/roles'
import { MOBILE_VERIFICATION_BASE_ROUTE } from '../constants/routeParts'

const PIXELS_TO_MM_RATIO = 2.8346456692913384
// DPI = 50

export interface Deferred {
  promise?: Promise<any>;
  resolve(): any;
  reject(): any;
}

export interface ScreenCoords {
  top: number;
  bottom: number;
  left: number;
  right: number;
}

export const toInt = (input: any): number => {
  if (!input) {
    return 0
  }

  if (typeof input === 'number') {
    return Math.floor(input)
  }

  const res = parseInt(input)

  return isNaN(res) ? 0 : res
}

export const getRenderContext = (page: PdfPage, canvas: HTMLCanvasElement):
  { canvasContext: CanvasRenderingContext2D, viewport: Rectangle } => {
  const scale = 1.0
  const viewport = page.getViewport(scale)
  const canvasContext = canvas.getContext('2d')

  canvas.height = viewport.height
  canvas.width = viewport.width

  return { canvasContext, viewport }
}

export const scale = (attr: number, real: number, curr: number): number => (
  toInt(attr * real / (curr * PIXELS_TO_MM_RATIO))
)

export const defer = (): Deferred => {
  const deferred = {}

  deferred.promise = new Promise<any, Error>((resolve, reject) => {
    deferred.resolve = resolve
    deferred.reject = reject
  })

  return deferred
}

export const pad = (number: number|string, neededQuantity: number = 2, padWith: string = '0'): string => {
  let r = number.toString()
  const length = r.length

  if (length === neededQuantity) {
    return r
  }

  for (let i = 0; i < neededQuantity - length; i++) {
    r = padWith + r
  }

  return r
}

export const clearCanvas = (canvas: HTMLCanvasElement): void => {
  if (canvas) {
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
  }
}

export const getDraggableOffset = (): number => (
  parseInt(window.getComputedStyle(document.body).getPropertyValue('--dragable-signature-padding'))
)

export const getImageParams = (image: Rectangle): Rectangle => {
  return {
    height: toInt(image.height),
    width: toInt(image.width)
  }
}

export const randomUserName = ((): (() => number) => {
  let res = 0

  const registry = {}
  const fn = (): number => Math.ceil(Math.random() * 100000)

  return (): number => {
    while (true) {
      res = fn()

      if (registry[res] === undefined) {
        registry[res] = true
        break
      }
    }

    return res
  }
})()

export const parseQueryParameters = (search: string): Map<string, string> => {
  const res = new Map()

  if (!search) {
    return res
  }

  if (search.charAt(0) === '?') {
    search = search.substr(1)
  }

  const parts = search.split('&')

  for (let i = 0; i < parts.length; i++) {
    const params = parts[i].split('=')

    for (let j = 0; j < params.length; j++) {
      res.set(params[0], params[1])
    }
  }

  return res
}

export const toObject = (map: Map<string, string>): Object => {
  const secParamsData = {}

  map.forEach((value, key) => {
    secParamsData[key] = value
  })

  return secParamsData
}

export const joinIfNotEmpty = (...input: Array<?string>): string => input.filter(p => p).join(' ')

export const getBase64FromDataUrl = (input: string): string|boolean => {
  for (let i = 0; i < input.length; i++) {
    if (input.charAt(i) === ',') {
      return input.slice(i + 1)
    }
  }

  return false
}

export const tryExec = (fn: () => any, defaultValue: any = null): any => {
  try {
    return fn()
  } catch (e) {
    return defaultValue
  }
}

export const extractHttpErrorMessage = (e: any): string => {
  const locations = [
    () => e.response.data.error,
    () => e.message
  ]

  for (let i = 0; i < locations.length; i++) {
    let res = locations[i]()

    if (res) {
      return res
    }
  }

  return e
}

export const insertOneChild = (el: HTMLElement, container: HTMLElement): HTMLElement|null => {
  if (container && container.children.length === 0) {
    return container.appendChild(el)
  }

  return null
}

export const getAudioContainerHandler = (): (() => HTMLElement|null) => {
  let cachedAudioContainer = null

  return () => {
    if (!cachedAudioContainer) {
      cachedAudioContainer = document.getElementById(AUDIO_TRACKS_CONTAINER_ID)
    }

    return cachedAudioContainer
  }
}

export const extractPathFromUrl = (url: string, { withLeadingSlash = false }: Object = {}): string => {
  let res
  const a = document.createElement('a')

  a.href = url
  res = a.pathname

  if (withLeadingSlash === false) {
    res = res.substr(1)
  }

  return res
}

const getDocumentKey = (key: string|null, postfix: string): string|null => {
  if (!key) {
    return null
  }

  const parts = key.split('.')
  const ext = parts.pop()

  return `${parts.join('.')}-${postfix}.${ext}`
}

export const getSignedDocumentKey = ({ key }: Object): string|null => getDocumentKey(key, 'signed')

export const getSealedDocumentKey = ({ key }: Object): string|null => getDocumentKey(key, 'sealed')

export const getCurrentDocument = (documents: Array<NotaryDocument>): NotaryDocument|null => {
  if (documents.length === 1) {
    return documents[0]
  }

  for (let i = 0; i < documents.length; i++) {
    if (PROCESSED_STATUSES.indexOf(documents[i].status) === -1) {
      return documents[i]
    }
  }

  return null
}

export const isLastDocumentUnprocessed = (documents: Array<Object>): boolean => {
  if (documents.length === 1) {
    return true
  }

  let unprocessedDocumentsCounter = 0

  for (let i = 0; i < documents.length; i++) {
    if (PROCESSED_STATUSES.indexOf(documents[i].status) === -1) {
      unprocessedDocumentsCounter++
    }

    if (unprocessedDocumentsCounter >= 2) {
      return false
    }
  }

  return unprocessedDocumentsCounter === 1
}

export const waitForZopim = (window: any): Promise<any> => (
  new Promise((resolve, reject) => {
    let iterations = 0
    let maxIterations = 1000

    const intervalRef = setInterval(() => {
      if (iterations >= maxIterations) {
        return reject(new Error('Couldn\'t wait for Zopim live chat to be loaded'))
      }

      if (window.$zopim === undefined || window.$zopim.livechat === undefined) {
        iterations++
        return
      }

      clearInterval(intervalRef)
      resolve(window.$zopim)
    }, 100)
  })
)

export const showSupportChatButton = async (window: any): Promise<any> => {
  const zopim = await waitForZopim(window)

  zopim.livechat.button.show()
}

export const extractFirstAndLastName = (input: string): any => {
  let firstName = null
  let lastName = null

  if (!input) {
    return { firstName, lastName }
  }

  const parts = input.split(' ')

  if (parts.length === 0) {
    return { firstName, lastName }
  }

  firstName = parts[0]

  if (parts.length > 1) {
    lastName = parts[parts.length - 1]
  }

  return { firstName, lastName }
}

export const getFormattedDate = (format: string = DATE_FORMAT_FULL, targetDate: Date = new Date()): string|number => {
  switch (format) {
    case DATE_FORMAT_FULL:
      return `${MONTHS[targetDate.getMonth()]} ${targetDate.getDate()}, ${targetDate.getFullYear()}`
    case DATE_FORMAT_MONTH_AND_DAY: return `${MONTHS[targetDate.getMonth()]} ${targetDate.getDate()}`
    case DATE_FORMAT_MONTH: return MONTHS[targetDate.getMonth()]
    case DATE_FORMAT_YEAR: return targetDate.getFullYear().toString()
    case DATE_FORMAT_DAY:
      const date = targetDate.getDate()

      if ([1, 21, 31].indexOf(date) >= 0) {
        return `${date}st`
      } else if ([2, 22].indexOf(date) >= 0) {
        return `${date}nd`
      } else if ([3, 23].indexOf(date) >= 0) {
        return `${date}rd`
      }

      return `${date}th`
    default: return targetDate.getFullYear()
  }
}

export const everyFirstToUpper = (text: string|null) => {
  if (!text) {
    return ''
  }

  return text.toLowerCase()
    .split(' ')
    .map(s => `${s.charAt(0).toUpperCase()}${s.substring(1)}`)
    .join(' ')
}

export const areArraysEq = (a: Array<any>, b: Array<any>): boolean => {
  if (!a || !b) {
    return false
  }

  if (a.length !== b.length) {
    return false
  }

  for (let i = 0; i < a.length; i++) {
    if (a[i] !== b[i]) {
      return false
    }
  }

  return true
}

export const loadScript = (path: string): Promise<any> => new Promise<any>((resolve) => {
  const head = document.getElementsByTagName('head')[0]
  const script = document.createElement('script')

  script.type = 'text/javascript'
  script.src = path
  script.addEventListener('load', resolve)
  head.appendChild(script)
})

export const truncate = (str: string, length: number): string => (
  (str.length > length) ? str.slice(0, length - 1) + '…' : str
)

export const sortBy = (collection: Array<any>, prop: string): Array<any> => (
  collection.sort((a, b) => {
    if (a[prop] < b[prop]) {
      return -1
    }

    if (a[prop] > b[prop]) {
      return 1
    }

    return 0
  })
)

export const scaleRectangle = (rectangle: Rectangle, minWidth: number): Rectangle => {
  let { height, width } = rectangle

  if (width < minWidth) {
    const ratio = width / minWidth

    width = minWidth
    height = toInt(ratio * height)
  }

  return { width, height }
}

export const parseBase64Image = (data: string): { contentType: string, data: string } => {
  const parts = data.split(';')

  return {
    contentType: parts[0].split(':')[1],
    data: parts[1].split(',')[1]
  }
}

export const b64toBlob = (b64Data: string, contentType: string = '', sliceSize: number = 512) => {
  const byteCharacters = atob(b64Data)
  const byteArrays = []

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize)

    const byteNumbers = new Array(slice.length)
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i)
    }

    const byteArray = new Uint8Array(byteNumbers)
    byteArrays.push(byteArray)
  }

  return new Blob(byteArrays, { type: contentType })
}

export const createImageUrl = (imageData: string): string => {
  const { contentType, data } = parseBase64Image(imageData)
  const blob = b64toBlob(data, contentType)

  return URL.createObjectURL(blob)
}

export const getElementCoordsById = (id: string): ScreenCoords|null => {
  const el = document.getElementById(id)

  if (!el) {
    return null
  }

  const top = el.offsetTop
  const left = el.offsetLeft

  return {
    top,
    left,
    bottom: top + el.offsetHeight,
    right: left + el.offsetWidth
  }
}

export const getScreenSize = (): Rectangle => ({
  // $FlowFixMe
  width: window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth || 0,
  // $FlowFixMe
  height: window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || 0
})

export const waitFor = (fn: () => boolean, intervalMs: number = 100): Promise<any> => (
  new Promise((resolve, reject) => {
    let iterations = 0
    let maxIterations = 1000

    const intervalRef = setInterval(() => {
      if (iterations >= maxIterations) {
        return reject(new Error('Couldn\'t wait entity to be loaded'))
      }

      if (!fn()) {
        iterations++
        return
      }

      clearInterval(intervalRef)
      resolve(window.$zopim)
    }, intervalMs)
  })
)

export const getOriginalImageSizeByUrl = (url: string): Promise<Rectangle> => (
  new Promise((resolve, reject) => {
    const newImg = new Image()

    newImg.onload = () => {
      const height = newImg.height
      const width = newImg.width

      return resolve({ width, height })
    }
    newImg.onerror = reject
    newImg.src = url
  })
)

export const getTextSize = (text: string, fontSize: number, maxWidth?: number): ?Rectangle => {
  const el = document.createElement('div')
  const app = document.getElementById('app')

  if (!app) {
    return
  }

  const css = [
    ['visibility', 'hidden'],
    ['position', 'fixed'],
    ['font-size', `${fontSize}px`],
    ['max-width', maxWidth ? `${maxWidth}px` : 'auto']
  ]

  el.id = Math.random() + ''
  el.innerHTML = text
  el.style.cssText = css.reduce((all, [attr, val]) => `${all}; ${attr}: ${val}`, '')

  app.appendChild(el)

  const size = {
    width: el.offsetWidth,
    height: el.offsetHeight
  }

  el.remove()

  return size
}

export const normalizeUrl = (url: string): string => url.replace(/(\/){2,}/g, '/')

export const getCurrentRole = (): string => {
  // TEMPORARY CODE. Should be token dependant
  if (window.location.hash.indexOf('official') >= 0) {
    return ROLE_OFFICIAL
  }

  // TEMPORARY CODE. Should be token dependant
  if (window.location.hash.indexOf(MOBILE_VERIFICATION_BASE_ROUTE) >= 0) {
    return ROLE_MOBILE_VERIFICATION_USER
  }

  return ROLE_USER
}

export const normalizeDate = (input: Object): Date|Object => {
  if (input.toDate) {
    return input.toDate()
  }

  if (input._seconds) {
    return new Date(input._seconds * 1000)
  }

  return input
}

export const makeUUID = (): string => (
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = Math.random() * 16 | 0
    const v = c === 'x' ? r : r & 0x3 | 0x8

    return v.toString(16)
  })
)

export const isFunction = (fn: any): boolean => (
  fn && {}.toString.call(fn) === '[object Function]'
)
