module.exports = {
  devServer: {
    disableHostCheck: false
  },
  pluginOptions: {
    webpackBundleAnalyzer: {
      openAnalyzer: false,
      analyzerMode: 'disabled'
    }
  },
  chainWebpack: config => {
    config.plugins.delete('preload')
  },
  pages: {
    index: {
      entry: 'src/main.js',
      template: 'public/index.html',
      filename: 'index.html'
    },
    test: {
      entry: 'src/test.js',
      template: 'public/test.html',
      filename: 'test.html'
    }
  }
}
