export default class TooltipCollection {
  constructor (tooltips) {
    this._tooltips = {}
    this._defaultDuration = 10 * 1000

    for (let i = 0; i < tooltips.length; i++) {
      const tooltip = typeof tooltips[i] === 'string' ? { name: tooltips[i] } : tooltips[i]

      tooltip.timeoutId = null
      tooltip.isVisible = false

      this._tooltips[tooltip.name] = tooltip
    }
  }

  getTooltip (name) {
    if (!this._tooltips[name]) {
      throw new Error(`No tooltip with name ${name} created.`)
    }

    return this._tooltips[name]
  }

  showTooltip (tooltipName) {
    const tooltip = this.getTooltip(tooltipName)
    const tooltipNames = Object.keys(this._tooltips)

    for (let i = 0; i < tooltipNames.length; i++) {
      const curr = this.getTooltip(tooltipNames[i])

      if (curr.isVisible) {
        this.hideTooltip(curr.name)
      }
    }

    tooltip.isVisible = true

    if (tooltip.visibilityDuration !== false) {
      tooltip.timeoutId = setTimeout(
        () => this.hideTooltip(tooltipName),
        tooltip.visibilityDuration || this._defaultDuration
      )
    }

    return this
  }

  hideTooltip (tooltipName) {
    const tooltip = this.getTooltip(tooltipName)

    if (tooltip.timeoutId) {
      clearTimeout(tooltip.timeoutId)
      tooltip.timeoutId = null
    }

    tooltip.isVisible = false

    if (tooltip.onHide) {
      tooltip.onHide(this)
    }

    return this
  }
}
