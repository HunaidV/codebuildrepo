import { ALL_ROLES, ROLE_USER, ROLE_OFFICIAL } from '../../constants/roles'

export default class StageRouteMap {
  constructor ({ routes, role }) {
    this.routeMap = {}
    this.role = role

    for (let i = 0; i < ALL_ROLES.length; i++) {
      this.routeMap[ALL_ROLES[i]] = []
    }

    for (let i = 0; i < routes.length; i++) {
      if (!routes[i] || !routes[i].meta) {
        continue
      }

      this._parseRoute(routes[i])
    }
  }

  getRouteForStage (stage) {
    if (!stage) {
      return null
    }

    return (this.routeMap[this.role] && this.routeMap[this.role][stage]) || null
  }

  _parseRoute (route) {
    if (route.meta.allowedOfficialStages) {
      for (let i = 0; i < route.meta.allowedOfficialStages.length; i++) {
        this.routeMap[ROLE_OFFICIAL][route.meta.allowedOfficialStages[i]] = route
      }
    }

    // Old "allowedStages" format
    if (route.meta.allowedStages instanceof Array) {
      for (let i = 0; i < route.meta.allowedStages.length; i++) {
        this.routeMap[ROLE_USER][route.meta.allowedStages[i]] = route
      }

      return this
    }

    // "allowedStages" with roles format
    if (route.meta.allowedStages instanceof Object) {
      const roles = Object.keys(route.meta.allowedStages)

      for (let i = 0; i < roles.length; i++) {
        const role = roles[i]

        for (let j = 0; j < route.meta.allowedStages[role].length; j++) {
          const stage = route.meta.allowedStages[role]

          this.routeMap[role][stage] = route
        }
      }
    }

    return this
  }
}
