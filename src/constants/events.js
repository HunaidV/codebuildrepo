export const AUTO_HIDE_ERROR = 'onAutoHideError'
export const SIGNING_RULES_CHECKED = 'onSigningRulesChecked'
export const ID_VERIFICATION_SDK_FAILURE = 'idVerificationSdkFailure'
export const IMAGE_ADDED_EVENT = 'IMAGE_ADDED'
export const IMAGE_ADDED_SIGNATURE_EVENT = `${IMAGE_ADDED_EVENT}.SIGNATURE`
export const IMAGE_ADDED_SEAL_EVENT = `${IMAGE_ADDED_EVENT}.RECTANGULARSEAL`
export const TEXT_ADDED_EVENT = 'TEXT_ADDED'
export const OPEN_NOTARY_FORM_EVENT = 'OPEN_NOTARY_FORM'
export const SIGNATURE_BUTTON_CLICKED_EVENT = 'SIGNATURE_BUTTON_CLICKED'
export const CREATE_TEXT_NODES_EVENT = 'onCreateTextNodesEvent'
export const CANCEL_TEXT_NODES_EVENT = 'onCancelTextNodesEvent'
export const RECORD_STARTED_EVENT = 'RECORD_STARTED'
export const RECORD_FINISHED_EVENT = 'RECORD_FINISHED'
export const STOP_RECORDING_COMMAND = 'STOP_RECORDING'
export const ATTACH_RECORDER_LINK_COMMAND = 'ATTACH_RECORDER_LINK'
