// @flow
import { Store } from 'vuex'
import Router, { Route } from 'vue-router'

import type { Deferred } from '../utils'
import {
  defer, parseQueryParameters, loadScript,
  toObject, getCurrentRole
} from '../utils'
import { NotarySession } from '../NotarySession'
import RemoteStorage from '../RemoteStorage'
import StageWatcher from './StageWatcher'
import StageRouteMap from './StageRouteMap'
import { NOTARY_SESSION_KEY } from '../../constants/remoteStorage'
import { DYNAMIC_RESOURCES } from '../../constants/dynamicResources'
import type { AsyncHook, Hook } from '../../interfaces/Hooks'

class RouteGuard {
  initialStagePromise: Deferred
  verifySecurityPromise: Promise<any>
  stageRouteMap: StageRouteMap
  router: Router
  store: Store
  afterAuthHooks: Array<Hook | AsyncHook> = []

  constructor () {
    const securityParams = parseQueryParameters(window.location.search)
    const notarySession = NotarySession.init()

    // Sets initial session ID from the URL
    if (securityParams.get('sid')) {
      notarySession.setSessionId(securityParams.get('sid'))
    }

    // Sets initial token from the URL
    if (securityParams.get('token')) {
      notarySession.setToken(securityParams.get('token'))
    }

    this.store = null
    this.router = null
    this.stageRouteMap = null
    this.initialStagePromise = defer()

    if (securityParams.size === 0) {
      this.verifySecurityPromise = Promise.resolve({ e: new Error('No security parameters detected') })
      return
    }

    this.verifySecurityPromise = notarySession
      .verifySecurityParams({
        ...toObject(securityParams),
        role: this.getCurrentRole()
      })
      .then((data) => {
        let sessionToken = null

        if (data && data.response) {
          const { response: { session: { id, officialUid, firmId, isPersonallyKnown, user,
            paymentMode, rate, payerEmail }, token } } = data

          sessionToken = token
          notarySession.setOfficialId(officialUid)
            .setSessionId(id)
            .setFirmId(firmId)
            .setIsPersonallyKnown(isPersonallyKnown)
            .setRole(this.getCurrentRole())
            .setUser(user)
            .setMode(paymentMode)
            .setPayerEmail(payerEmail)
            .setRate(rate)
        }

        return { ...data, token: sessionToken }
      })
  }

  async createStorePlugin (store: Store): Promise<void> {
    this.store = store
    await this.watch()
  }

  registerRouter (router: Router): void {
    this.router = router
    this.buildStageRouteMap()
    this.watch()
  }

  buildStageRouteMap (): void {
    this.stageRouteMap = new StageRouteMap({
      routes: this.router.options.routes,
      role: this.getCurrentRole()
    })
  }

  watch (): Promise<Array<any>>|void {
    if (!this.router || !this.store) {
      return
    }

    const res = Promise.all([
      this.createStateWatcher(),
      this.createRouteWatcher(),
      this.preLoadDynamicResources()
    ])

    res.then(() => this.afterAuthHooks.map(hook => hook(this.store, {
      role: this.getCurrentRole()
    })))

    return res
  }

  async createRouteWatcher (): Promise<void> {
    this.router.beforeEach(async (to, from, next) => {
      window.siteLoaded && window.siteLoaded()

      if (to.meta && to.meta.serviceRoute) {
        return next()
      }

      try {
        const { res, e } = await this.verifySecurityPromise

        if (!res) {
          this.handleInvalidSecurity(e)

          return next(this.getForbiddenRoute())
        }

        if (!this.store.state.stage) {
          await this.initialStagePromise.promise
        }

        const isRouteAllowed = this.isRouteAllowed(to)

        if (isRouteAllowed) {
          return next()
        }
      } catch (e) {
        return next(this.getForbiddenRoute())
      }

      const allowedRoute = this.stageRouteMap.getRouteForStage(this.store.state.stage)

      if (allowedRoute && allowedRoute.path) {
        return next({ path: allowedRoute.path })
      }

      return next(false)
    })
  }

  async createStateWatcher (): Promise<any> {
    const { res, e, token } = await this.verifySecurityPromise

    if (!res) {
      return this.handleInvalidSecurity(e)
    }

    StageWatcher.create({
      store: this.store,
      stageRouteMap: this.stageRouteMap,
      router: this.router
    })

    const sessionId = NotarySession.init().getSessionId()
    const db = await RemoteStorage(token)

    return new Promise((resolve, reject) => (
      db.collection(NOTARY_SESSION_KEY)
        .doc(sessionId)
        .onSnapshot((doc) => {
          const data = doc.data()
          const initialSetting = this.store.state.stage === null

          this.store.commit('changeState', {
            ...data,
            sessionId: doc.id,
            role: this.getCurrentRole()
          })

          if (initialSetting) {
            this.initialStagePromise.resolve()
          }

          resolve()
        }, reject)
    ))
  }

  async preLoadDynamicResources (): Promise<void> {
    const currentRole = this.getCurrentRole()

    if (!DYNAMIC_RESOURCES[currentRole]) {
      return
    }

    if (typeof DYNAMIC_RESOURCES[currentRole] === 'function') {
      await DYNAMIC_RESOURCES[currentRole]()
    }

    if (Array.isArray(DYNAMIC_RESOURCES[currentRole])) {
      const promises = DYNAMIC_RESOURCES[currentRole]
        .map((path: string): Promise<any> => loadScript(path))

      await Promise.all(promises)
    }
  }

  isRouteAllowed (route: Route) {
    if (route.meta && route.meta.serviceRoute) {
      return true
    }

    const allowedRoute = this.stageRouteMap.getRouteForStage(this.store.state.stage)

    if (!allowedRoute) {
      return false
    }

    return route.name === allowedRoute.name
  }

  getCurrentRole (): string {
    return getCurrentRole()
  }

  getForbiddenRoute (): { [key: string]: string } {
    return { path: '/forbidden' }
  }

  handleInvalidSecurity (e: Error): void {
    return console.error(e)
  }

  addAfterAuthHook (hook: Hook | AsyncHook): this {
    this.afterAuthHooks.push(hook)

    return this
  }
}

export default new RouteGuard()
