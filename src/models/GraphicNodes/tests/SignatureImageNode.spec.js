import { toInt } from '../../../services/utils'
import SignatureImageNode from '../SignatureImageNode'

const docParams = { width: 100, height: 100 }
const signatureParams = { width: 80, height: 80 }

describe('SignatureImageNode', () => {
  describe('scaleUp', () => {
    it('Should increase image height by 10 pixels', () => {
      const node = new SignatureImageNode().setParentDocument(docParams)

      node.setParameters(signatureParams)
      node.scaleUp()

      expect(node.height).toBe(signatureParams.height + 10)
    })

    it('Should increase image width proportionally', () => {
      const node = new SignatureImageNode().setParentDocument(docParams)

      node.setParameters(signatureParams)

      const ratio = (node.height + 10) / node.height
      const expectedWidth = toInt(node.width * ratio)

      node.scaleUp()

      expect(node.width).toBe(expectedWidth)
    })
  })

  describe('scaleDown', () => {
    it('Should decrease image height by 10 pixels', () => {
      const node = new SignatureImageNode().setParentDocument(docParams)

      node.setParameters(signatureParams)
      node.scaleDown()

      expect(node.height).toBe(signatureParams.height - 10)
    })

    it('Should decrease image width proportionally', () => {
      const node = new SignatureImageNode().setParentDocument(docParams)

      node.setParameters(signatureParams)

      const ratio = (node.height - 10) / node.height
      const expectedWidth = toInt(node.width * ratio)

      node.scaleDown()

      expect(node.width).toBe(expectedWidth)
    })
  })
})
