export default class BasicStorage {
  constructor (storage) {
    this._storage = storage
  }

  _get (key, defaultValue = null) {
    try {
      return JSON.parse(this._storage.getItem(key)) || defaultValue
    } catch (e) {
      return defaultValue
    }
  }

  _set (key, val) {
    this._storage.setItem(key, JSON.stringify(val))
  }

  _remove (key) {
    this._storage.removeItem(key)
  }
}
