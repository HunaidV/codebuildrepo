// @flow
import { getSignedDocumentKey, getSealedDocumentKey } from '../../services/utils'

interface DocumentLinksData {
  file: string,
  key: string
}

export default class DocumentLinks {
  bucket: string
  document: ?DocumentLinksData

  constructor () {
    // $FlowFixMe
    this.bucket = process.env.VUE_APP_S3_BUCKET
    this.document = null
  }

  setDocument (document: DocumentLinksData): this {
    this.document = document

    return this
  }

  get original (): ?string {
    if (!this.document) {
      return null
    }

    return this.document.file
  }

  get signed (): ?string {
    if (!this.document) {
      return null
    }

    return this._generateS3Url(getSignedDocumentKey(this.document))
  }

  get sealed (): ?string {
    if (!this.document) {
      return null
    }

    return this._generateS3Url(getSealedDocumentKey(this.document))
  }

  _generateS3Url (key: ?string): ?string {
    if (key) {
      return `https://${this.bucket}.s3.amazonaws.com/${key}`
    }

    return null
  }
}
