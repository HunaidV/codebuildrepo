export interface SignatureBoxPointer {
  width: number;
  height: number;
  left: number;
  top: number;
  pageNumber: number;
  canvas: {
    width: number;
    height: number;
  }
}
