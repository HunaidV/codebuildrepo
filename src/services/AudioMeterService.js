export default class AudioMeterService {
  constructor (stream) {
    const audioContext = this._getAudioContext()
    const analyser = audioContext.createAnalyser()
    const microphone = audioContext.createMediaStreamSource(stream)
    const javascriptNode = audioContext.createScriptProcessor(2048, 1, 1)

    analyser.smoothingTimeConstant = 0.8
    analyser.fftSize = 1024
    microphone.connect(analyser)
    analyser.connect(javascriptNode)
    javascriptNode.connect(audioContext.destination)

    javascriptNode.onaudioprocess = () => {
      if (!this.onChangeLevelCallback) {
        return
      }

      let values = 0
      let array = new Uint8Array(analyser.frequencyBinCount)

      analyser.getByteFrequencyData(array)

      let length = array.length
      for (let i = 0; i < length; i++) {
        values += (array[i])
      }

      let average = Math.ceil(values / length * 100) / 10000

      this.onChangeLevelCallback(average)
    }
  }

  onChangeLevel (fn) {
    this.onChangeLevelCallback = fn

    return this
  }

  _getAudioContext () {
    if (window.AudioContext) {
      return new AudioContext()
    }

    if (window.webkitAudioContext) {
      // eslint-disable-next-line
      return new window.webkitAudioContext()
    }

    throw new Error('AudioContext not found')
  }
}
