// @flow
export const MONTHS: Array<string> = [
  'January', 'February', 'March',
  'April', 'May', 'June',
  'July', 'August', 'September',
  'October', 'November', 'December'
]
export const DATE_FORMAT_FULL = 'full'
export const DATE_FORMAT_MONTH_AND_DAY = 'monthAndDay'
export const DATE_FORMAT_DAY = 'day'
export const DATE_FORMAT_MONTH = 'month'
export const DATE_FORMAT_YEAR = 'year'
export const DATES = (() => {
  const dates = []

  for (let i = 0; i < 31; i++) {
    dates.push(i + 1)
  }

  return dates
})()
export const generateReverseYearsArray = (from: number, to?: number|null = null): Array<number> => {
  if (!to) {
    to = (new Date()).getFullYear()
  }

  if (from > to) {
    const temp = to

    to = from
    from = temp
  }

  const res = [to]

  for (let i = to; i >= from; i--) {
    res.push(i)
  }

  return res
}
