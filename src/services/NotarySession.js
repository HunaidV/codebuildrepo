import axios from 'axios'

import { parseQueryParameters, sortBy, toObject } from './utils'
import RemoteStorage from './RemoteStorage'
import {
  getNextStage, getPrevStage, getFirstStage,
  SESSION_CANCELLED
} from '../constants/stages'
import { COMPLETED } from '../constants/sessionStatuses'
import { ROLE_MOBILE_VERIFICATION_USER } from '../constants/roles'
import { NOTARY_SESSION_KEY, NOTARY_RECORDS_KEY } from '../constants/remoteStorage'
import { DEFAULT_FLOW, KBA_FLOW, MOBILE_ID_VERIFICATION_FLOW } from '../constants/flows'
import { DEFAULT_MODE } from '../constants/sessionModes'
import { sendLogs } from '../plugins/requestsLogging'

let cached = null

export class NotarySession {
  static init () {
    if (!cached) {
      cached = new NotarySession()
    }

    return cached
  }

  constructor () {
    this.apiBaseUrl = process.env.VUE_APP_API_BASE_URL
    this.digitalSigningEndpointApiUrl = process.env.VUE_APP_DIGITAL_SIGNING_API_ENDPOINT_URL
    this.flow = DEFAULT_FLOW
  }

  setUser (user) {
    this.user = user

    return this
  }

  getUser () {
    return this.user
  }

  setMode (mode) {
    this.mode = mode

    return this
  }

  getMode () {
    const mode = this.mode || DEFAULT_MODE

    return mode.trim().toLowerCase()
  }

  setPayerEmail (email) {
    this.payerEmail = email

    return this
  }

  getPayerEmail () {
    return this.payerEmail
  }

  setToken (token) {
    this.token = token

    return this
  }

  setRate (rate) {
    this.rate = rate

    return this
  }

  getRate () {
    return this.rate
  }

  setSessionId (sessionId) {
    this.uid = sessionId

    return this
  }

  setOfficialId (officialId) {
    this.notaryUid = officialId

    return this
  }

  setFirmId (firmId) {
    this.firmId = firmId

    return this
  }

  setIsPersonallyKnown (isPersonallyKnown) {
    this.flow = this.getFlowFromSessionType(isPersonallyKnown)

    return this
  }

  setRole (role) {
    if (role === ROLE_MOBILE_VERIFICATION_USER) {
      this.flow = MOBILE_ID_VERIFICATION_FLOW
    }

    return this
  }

  getFlowFromSessionType (isPersonallyKnown) {
    switch (isPersonallyKnown) {
      case false: return KBA_FLOW
      case true: return DEFAULT_FLOW
      default: return DEFAULT_FLOW
    }
  }

  isKBAFlow () {
    return this.flow === KBA_FLOW
  }

  isPKFlow () {
    return this.flow !== KBA_FLOW
  }

  isMobileVerificationFlow () {
    return this.flow === MOBILE_ID_VERIFICATION_FLOW
  }

  generateCredentials () {
    return {
      ...this.generateSignature(),
      ...this.generateOfficialId(),
      ...this.generateFirmId()
    }
  }

  generateSignature () {
    return { 'X-SESSION-ID': this.uid }
  }

  generateOfficialId () {
    return { 'X-OFFICIAL-ID': this.notaryUid }
  }

  generateFirmId () {
    return { 'X-FIRM-ID': this.firmId }
  }

  async sendPaymentLinkEmail () {
    const { data } = await axios.post(
      `${this.apiBaseUrl}/payment/email`,
      { token: this.token },
      { headers: this.generateSignature() }
    )

    return data
  }

  async sign (requestData) {
    const { data } = await axios.post(this.digitalSigningEndpointApiUrl, requestData)

    return data
  }

  async getOfficialFiles () {
    const requestUrl = `${this.apiBaseUrl}/files`
    const params = { headers: { ...this.generateSignature(), ...this.generateOfficialId() } }
    const { data } = await axios.get(requestUrl, params)

    return data
  }

  async seal (requestData) {
    const { data } = await axios.post(this.digitalSigningEndpointApiUrl, requestData)

    return data
  }

  async getSignatureInfo (requestData) {
    const requestUrl = `${this.apiBaseUrl}/signature-info`
    const params = { headers: { ...this.generateCredentials() } }
    const { data } = await axios.post(requestUrl, requestData, params)

    return data
  }

  async geOfficialInfo () {
    const requestUrl = `${this.apiBaseUrl}/officials/${this.notaryUid}`
    const params = { headers: { ...this.generateCredentials() } }
    const { data } = await axios.get(requestUrl, params)

    return data
  }

  async verifySecurityParams (params) {
    const keys = Object.keys(params)
    const queryParams = []

    for (let i = 0; i < keys.length; i++) {
      queryParams.push(`${keys[i]}=${params[keys[i]]}`)
    }

    const requestUrl = `${this.apiBaseUrl}/sessions/verify?${queryParams.join('&')}`

    try {
      const { data } = await axios.get(requestUrl, { withCredentials: true })

      return { res: true, response: data }
    } catch (e) {
      return { res: false, e }
    }
  }

  async getSessionErrorLogs (): Promise<any> {
    const securityParams = parseQueryParameters(window.location.search)

    return this.verifySecurityParams({
      ...toObject(securityParams),
      with: 'errorLogs'
    })
  }

  async getScreenRecordingCheckUrl (): Promise<string> {
    const securityParams = parseQueryParameters(window.location.search)
    const { response: { screenShareVerifyLink } } = await this.verifySecurityParams({
      ...toObject(securityParams),
      with: 'screenShareVerifyLink'
    })

    return screenShareVerifyLink
  }

  async isScreenRecordingUploaded (url: string): Promise<boolean> {
    try {
      await axios.head(url)

      return true
    } catch (e) {
      return false
    }
  }

  getSessionId () {
    return this.uid
  }

  async getDocuments () {
    const requestUrl = `${this.apiBaseUrl}/documents?session-id=${this.getSessionId()}`
    const params = { headers: this.generateFirmId() }
    const { data } = await axios.get(requestUrl, params)

    return sortBy(data, 'name')
  }

  async getNotaryRecords () {
    const db = await RemoteStorage()

    const records = await db.collection(NOTARY_RECORDS_KEY)
      .where('sessionId', '==', db.doc(`${NOTARY_SESSION_KEY}/${this.getSessionId()}`))
      .get()

    if (records.docs && records.docs.length) {
      return records.docs
    }

    return []
  }

  async saveNotaryRecord (input) {
    let currentNotaryRecordId
    const sessionNotaryRecords = await this.getNotaryRecords()
    const db = await RemoteStorage()

    for (let i = 0; i < sessionNotaryRecords.length; i++) {
      const data = sessionNotaryRecords[i].data()

      if (data && data.documentId === input.documentId) {
        currentNotaryRecordId = sessionNotaryRecords[i].id
      }
    }

    if (!currentNotaryRecordId) {
      const data = {
        ...input,
        firmId: this.firmId,
        officialId: this.notaryUid,
        sessionId: db.doc(`${NOTARY_SESSION_KEY}/${this.getSessionId()}`)
      }

      return db.collection(NOTARY_RECORDS_KEY).add(data)
    }

    return this.updateNotaryRecord(input, currentNotaryRecordId)
  }

  async updateNotaryRecord (input, id) {
    const db = await RemoteStorage()
    const data = {
      ...input,
      firmId: this.firmId,
      officialId: this.notaryUid,
      sessionId: db.doc(`${NOTARY_SESSION_KEY}/${this.getSessionId()}`)
    }

    return db.collection(NOTARY_RECORDS_KEY)
      .doc(id)
      .update(data)
  }

  async updateSession (input) {
    const sessionId = this.getSessionId()
    const db = await RemoteStorage()

    return db.collection(NOTARY_SESSION_KEY)
      .doc(sessionId)
      .update(input)
  }

  async sendNotification (status = COMPLETED) {
    const requestUrl = `${this.apiBaseUrl}/notifications`
    const requestData = {
      status,
      sessionId: this.getSessionId()
    }
    const params = { headers: { ...this.generateCredentials() } }
    const { data } = await axios.post(requestUrl, requestData, params)

    return data
  }

  async updateThroughAPI (sessionId, data) {
    const requestUrl = `${this.apiBaseUrl}/sessions/${sessionId}`
    const params = { headers: { ...this.generateCredentials() } }
    const { data: response } = await axios.put(requestUrl, data, params)

    return response
  }

  async commitStage (stage, extra = {}) {
    return NotarySession.commitStage(stage, extra)
  }

  async cancel () {
    return NotarySession.commitStage(SESSION_CANCELLED, { cancelledAt: new Date() })
  }

  toPrevStage (currStage, extra = {}) {
    return NotarySession.toPrevStage(currStage, extra)
  }

  async addEvent (event, events) {
    const max = 10
    const sessionId = NotarySession.init().getSessionId()
    const db = await RemoteStorage()

    if (events && events.length >= max) {
      events = events.slice(Math.max(events.length - max, 1))
    }

    events.push(event)

    return db.collection(NOTARY_SESSION_KEY)
      .doc(sessionId)
      .update({ events })
  }

  async addCommand (command, commands) {
    const sessionId = NotarySession.init().getSessionId()
    const db = await RemoteStorage()

    commands.push({
      command,
      timestamp: new Date()
    })

    return db.collection(NOTARY_SESSION_KEY)
      .doc(sessionId)
      .update({ commands })
  }

  async updateCommands (commands) {
    const sessionId = NotarySession.init().getSessionId()
    const db = await RemoteStorage()

    return db.collection(NOTARY_SESSION_KEY)
      .doc(sessionId)
      .update({ commands })
  }

  async convertDocument (documentId, { stage }) {
    const requestUrl = `${this.apiBaseUrl}/documents/${documentId}/convert`
    const params = { headers: { ...this.generateCredentials() } }
    const { data } = await axios.post(requestUrl, { stage }, params)

    return data
  }

  async triggerComposition (recordingId: string) {
    const requestUrl = `${this.apiBaseUrl}/recordings/${recordingId}/compose`
    const params = { headers: { ...this.generateCredentials() } }
    const { data } = await axios.post(requestUrl, {}, params)

    return data
  }

  static async commitStage (stage: string, extra: Object = {}): Promise<any> {
    const sessionId = NotarySession.init().getSessionId()
    const db = await RemoteStorage()

    sendLogs({
      sessionId,
      component: 'Session',
      message: 'Session stage changed',
      level: 'debug',
      service: 'APP',
      userAgent: window.navigator.userAgent,
      extra: {
        newStage: stage,
        currentUrl: window.location.href
      }
    })

    return db.collection(NOTARY_SESSION_KEY)
      .doc(sessionId)
      .update({ stage, ...extra })
  }

  static toNextStage (currStage, extra = {}) {
    const flow = NotarySession.init().flow

    return NotarySession.commitStage(getNextStage(currStage, flow), extra)
  }

  static toPrevStage (currStage, extra = {}) {
    const flow = NotarySession.init().flow

    return NotarySession.commitStage(getPrevStage(currStage, flow), extra)
  }

  static toFirstStage (extra = {}) {
    const flow = NotarySession.init().flow

    return NotarySession.commitStage(getFirstStage(flow), extra)
  }

  static async changeSetting (key, val) {
    const sessionId = NotarySession.init().getSessionId()
    const db = await RemoteStorage()

    return db.collection(NOTARY_SESSION_KEY)
      .doc(sessionId)
      .update({ [`settings.${key}`]: val })
  }

  static setIdRetakeCounter (idRetakeCounter) {
    return NotarySession.init()
      .updateSession({ idRetakeCounter })
  }

  static async saveSignature (signatureImage) {
    const sessionId = NotarySession.init().getSessionId()
    const db = await RemoteStorage()

    return db.collection(NOTARY_SESSION_KEY)
      .doc(sessionId)
      .update({ signatureImage })
  }

  static async sendIdVerificationLink (requestData) {
    const session = NotarySession.init()

    const requestUrl = `${session.apiBaseUrl}/verification-link`
    const params = { headers: { ...session.generateCredentials() } }
    const { data } = await axios.post(requestUrl, requestData, params)

    return data
  }
}
