const Cancelled = () => import('@/views/official/Cancelled')
const OfficialComplete = () => import('@/views/official/OfficialComplete')
const OfficialInitial = () => import('@/views/official/OfficialInitial')
const OfficialReview = () => import('@/views/official/OfficialReview')
const OfficialSealing = () => import('@/views/official/OfficialSealing')

export {
  Cancelled,
  OfficialComplete,
  OfficialInitial,
  OfficialReview,
  OfficialSealing
}
