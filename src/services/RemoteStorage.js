import Firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

let connection = null
let cachedToken = null

const config = JSON.parse(process.env.VUE_APP_FIRESTORE_CONFIG)

const getConnection = async (token) => {
  if (token) {
    cachedToken = token
  }

  if (!cachedToken) {
    throw new Error('No Firestore token provided')
  }

  if (connection === null) {
    const firebaseApp = Firebase.initializeApp(config)
    const firestore = firebaseApp.firestore()

    await Firebase.auth().signInWithCustomToken(cachedToken)

    connection = firestore
  }

  return connection
}

export default getConnection
