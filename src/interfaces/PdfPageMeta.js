// @flow
import type { PositionedRectangle } from './PositionedRectangle'

export interface PdfPageMeta {
  pageNumber: number;
  realWidth: number;
  realHeight: number;
  canvas: PositionedRectangle;
}
