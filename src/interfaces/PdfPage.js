// @flow
import type { Rectangle } from './Rectangle'
import type { CanvasMeta } from './CanvasMeta'

export interface PdfPage {
  getViewport(scale: number): Rectangle;
  render(CanvasMeta): Promise<Rectangle>
}
