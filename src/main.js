import 'babel-polyfill'

import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

import './plugins/vuetify'
import './plugins/errorHandling'
import './plugins/requestsLogging'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import './main.css'
import { build } from './store'
import { MdProgress } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.config.productionTip = false
Vue.use(VueAnalytics, {
  router,
  id: process.env.VUE_APP_GOOGLE_ANALYTICS_ID
})
Vue.use(MdProgress)

const main = async () => (
  new Vue({
    store: await build(),
    router,
    render: r => r(App)
  }).$mount('#app')
)

main()
