module.exports = {
  presets: [
    '@vue/app',
    '@babel/preset-env'
  ],
  plugins: [
    'babel-plugin-transform-class-properties',
    'babel-plugin-syntax-flow',
    'babel-plugin-transform-flow-strip-types'
  ]
}
