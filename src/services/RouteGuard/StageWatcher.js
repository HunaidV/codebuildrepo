// @flow
import { Store } from 'vuex'
import Router, { Route } from 'vue-router'
import StageRouteMap from './StageRouteMap'

interface StageWatcherInput {
  store: Store,
  stageRouteMap: StageRouteMap,
  router: Router
}

export default class StageWatcher {
  static create ({ store, stageRouteMap, router }: StageWatcherInput): void {
    store.subscribe((mutation, state) => {
      const stage = state.stage
      const route = stageRouteMap.getRouteForStage(stage)
      const currentRoute = StageWatcher.getCurrentRoute(router)

      if (currentRoute && currentRoute.meta && currentRoute.meta.serviceRoute) {
        return
      }

      const currentRouteName = router.currentRoute.name

      if (route && currentRouteName && currentRouteName !== route.name) {
        router.push({ name: route.name })
      }
    })
  }

  static getCurrentRoute (router: Router): Route {
    if (router.currentRoute.name === null) {
      const rawUrl = window.location.hash.substr(1)

      return router.matcher.match(rawUrl)
    }

    return router.currentRoute
  }
}
