import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    info: '#006aff',
    accent: '#f4511e',
    accent2: '#d84315',
    ferrari: '#ff2800',
    successAlt: '#2ebe60'
  }
})
