export const ROLE_USER = 'user'
export const ROLE_OFFICIAL = 'official'
export const ROLE_MOBILE_VERIFICATION_USER = 'mobileVerificationUser'
export const ALL_ROLES = [
  ROLE_USER,
  ROLE_OFFICIAL,
  ROLE_MOBILE_VERIFICATION_USER
]
