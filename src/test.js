import 'babel-polyfill'

import Vue from 'vue'
import './plugins/vuetify'
import './plugins/errorHandling'
import TestApp from './TestApp.vue'
import './main.css'

const main = async () => {
  return new Vue({
    render: r => r(TestApp)
  }).$mount('#test-app')
}

main()
