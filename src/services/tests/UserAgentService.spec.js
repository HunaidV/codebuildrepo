import UserAgentService from '../UserAgentService'

describe('UserAgentService', () => {
  describe('decodeOS', () => {
    it('Should transcode user agent info into OS info', () => {
      const os = UserAgentService.decodeOS({
        appVersion: '5.0 (Windows)',
        platform: 'Win32',
        userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0'
      })

      expect(os).toBe('Windows 10')
    })
  })

  describe('decodeBrowser', () => {
    it('Should transcode user agent info into OS info', () => {
      const os = UserAgentService.decodeBrowser({
        appVersion: '5.0 (Windows)',
        platform: 'Win32',
        userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0'
      })

      expect(os).toBe('Firefox 69')
    })
  })
})
