// @flow
import Model from './Model'
import { toInt } from '../services/utils'
import { MONTHS } from '../constants/dates'

export interface PersonalDetailsData {
  firstName: string|null,
  lastName: string|null,
  address1: string|null,
  address2: string|null,
  city: string|null,
  state: string|null,
  zipCode: string|null,
  dob: string|null,
  dl: string|null,
  ssn: string|null
}

export default class PersonalDetailsModel extends Model {
  firstName: string|null = null
  lastName: string|null = null
  address1: string|null = null
  address2: string|null = null
  city: string|null = null
  state: string|null = null
  zipCode: string|null = null
  dob: string|null = null
  dl: string|null = null
  ssn: string|null = null
  month: string|null = null
  date: number|null = null
  year: number|null = null

  constructor () {
    super()
    this.setFormElements([
      {
        type: 'text',
        property: 'firstName',
        label: 'First name'
      }, {
        type: 'text',
        property: 'lastName',
        label: 'Last name'
      }, {
        type: 'text',
        property: 'address1',
        label: 'Address'
      }, {
        type: 'text',
        property: 'city',
        label: 'City'
      }, {
        type: 'text',
        property: 'state',
        label: 'State'
      }, {
        type: 'text',
        property: 'zipCode',
        label: 'ZIP Code'
      }, {
        type: 'text',
        property: 'dl',
        label: 'Driver\'s license number'
      }, {
        type: 'text',
        property: 'ssn',
        label: 'Last four SSN',
        required: false
      }
    ])
  }

  populate (input: Object = {}): this {
    const res = super.populate(input)

    if (this.dob) {
      this._splitDateOfBirth()
    }

    return res
  }

  getData (): PersonalDetailsData {
    this._joinDateOfBirth()

    return {
      firstName: this.firstName,
      lastName: this.lastName,
      address1: this.address1,
      address2: this.address2,
      city: this.city,
      state: this.state,
      zipCode: this.zipCode,
      dob: this.dob,
      dl: this.dl,
      ssn: this.ssn
    }
  }

  _splitDateOfBirth (): this {
    if (!this.dob) {
      return this
    }

    const parts = this.dob.split('-')
    const monthNumber = toInt(parts[1]) || null

    if (monthNumber !== null && MONTHS[monthNumber - 1]) {
      this.month = MONTHS[monthNumber - 1]
    }

    this.date = toInt(parts[2]) || null
    this.year = toInt(parts[0]) || null

    return this
  }

  _joinDateOfBirth (): this {
    if (this.month === null || this.month === undefined) {
      return this
    }

    let monthIndex = MONTHS.findIndex(el => el === this.month)

    if (monthIndex === -1) {
      monthIndex = 1
    }

    if (this.date && this.year) {
      const monthNumber = (monthIndex + 1).toString().padStart(2, '0')
      const date = this.date ? this.date.toString().padStart(2, '0') : ''

      this.dob = `${this.year || ''}-${monthNumber}-${date}`
    }

    return this
  }
}
