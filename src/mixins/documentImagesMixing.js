// @flow
import { mapState } from 'vuex'
import { OFFICIAL_SEALED, SESSION_ENDED, USER_SIGNED } from '../constants/stages'
import type { NotaryDocument } from '../interfaces/NotaryDocument'
import { areArraysEq } from '../services/utils'

export default {
  data () {
    return {
      documentImageUrls: null
    }
  },
  computed: {
    ...mapState(['stage']),
    isSigned (): boolean {
      return this.stage === USER_SIGNED
    },
    isSealed (): boolean {
      return this.stage === OFFICIAL_SEALED
    },
    isSessionEnded (): boolean {
      return this.stage === SESSION_ENDED
    }
  },
  methods: {
    async configureCurrentDocumentImages (documents?: Array<any>, currDoc?: any): Promise<void> {
      let doc
      const allDocs = documents || await this.$options.notarySession.getDocuments()
      const currentDocument = currDoc || this.$options.currentDocument

      for (let i = 0; i < allDocs.length; i++) {
        if (allDocs[i].id === currentDocument.id) {
          doc = allDocs[i]
        }
      }

      if (doc) {
        const documentImageUrls = this.getCurrentDocumentImages(doc)

        // Update URLs only if they changes to avoid a glitch
        if (areArraysEq(documentImageUrls, this.documentImageUrls) === false) {
          this.documentImageUrls = documentImageUrls
        }
      }
    },
    getCurrentDocumentImages (currentDocument: NotaryDocument) {
      if (this.isSigned) {
        return currentDocument.convertedImages.signed
      }

      if (this.isSealed || this.isSessionEnded) {
        return currentDocument.convertedImages.sealed
      }

      return currentDocument.convertedImages.original
    }
  }
}
