import GraphicNode from './GraphicNode'
import { scale, toInt } from '../../services/utils'

const sizes = {
  rectangularSeal: {
    minHeight: 20,
    maxHeight: 70,
    maxWidth: 179
  },
  circularSeal: {
    minHeight: 20,
    maxHeight: 141,
    maxWidth: 141
  },
  signature: {
    minHeight: 20
  }
}

export default class ImageNode extends GraphicNode {
  constructor () {
    super()
    this.imageId = null
    this.url = null
    this.type = null
    this.minHeight = null
    this.offset = 0
    this.imageAttributes = {
      height: 1,
      width: 200,
      loaded: false
    }
    this.defaultStickSize = 10
    this.toogleSticksEdge = 30
    this.stickSize = this.defaultStickSize
    this.documentWidth = 0
    this.documentHeight = 0
  }

  get containerWidth () {
    return this.imageAttributes.width + this.offset * 2
  }

  get containerHeight () {
    return this.imageAttributes.height + this.offset * 2
  }

  get isImageLoaded () {
    return this.imageAttributes.loaded
  }

  setType (type) {
    if (Object.keys(sizes).indexOf(type) <= -1) {
      throw new Error(`Invalid image node type: ${type}`)
    }

    this.type = type
    this.minHeight = sizes[this.type].minHeight

    return this
  }

  setImage (image) {
    this.image = image

    return this.resetImageAttributes()
  }

  setIndex (index) {
    this.id = `image-${index}`
    this.imageId = `image-file-${index}`

    return this
  }

  setUrl (url) {
    this.url = url

    return this
  }

  setDocumentWidth (documentWidth) {
    this.documentWidth = documentWidth

    return this
  }

  setDocumentHeight (documentHeight) {
    this.documentHeight = documentHeight

    return this
  }

  resetImageAttributes (params = null) {
    const { width, height } = params || this.image
    const { width: newWidth, height: newHeight } = this.getNormalizedSize({ width, height })

    this._setImageAttributes({ width: newWidth, height: newHeight })
      ._setImageSize({ width: newWidth, height: newHeight })
  }

  getNormalizedSize ({ height, width }) {
    const size = { height, width }

    if (sizes[this.type].maxHeight && size.height > sizes[this.type].maxHeight) {
      size.height = sizes[this.type].maxHeight
    }

    if (sizes[this.type].maxWidth && size.width > sizes[this.type].maxWidth) {
      size.width = sizes[this.type].maxWidth
    }

    return size
  }

  setOffset (offset) {
    this.offset = offset

    return this
  }

  calculatePosition ({
    realParentHeight, realParentWidth, currentParentHeight,
    currentParentWidth, currentTop, currentLeft, currentWidth, currentHeight
  }) {
    return {
      left: scale(currentLeft + this.offset, realParentWidth, currentParentWidth),
      top: scale(currentTop + this.offset, realParentHeight, currentParentHeight),
      height: scale(currentHeight, realParentHeight, currentParentHeight),
      width: scale(currentWidth, realParentWidth, currentParentWidth)
    }
  }

  get x () {
    return toInt(this.documentWidth * 2 / 3)
  }

  get y () {
    return toInt(this.documentHeight / 2 - 20)
  }

  _setImageSize ({ height, width }) {
    this.image.style.height = `${height}px`
    this.image.style.width = `${width}px`

    return this
  }

  _setImageAttributes ({ width, height }) {
    const { width: normalizedWidth, height: normalizedHeight } = this.getNormalizedSize({ width, height })

    this.imageAttributes.width = normalizedWidth
    this.imageAttributes.height = normalizedHeight

    if (this.imageAttributes.width < this.toogleSticksEdge || this.imageAttributes.height < this.toogleSticksEdge) {
      this.stickSize = 8
    } else {
      this.stickSize = this.defaultStickSize
    }

    if (this.imageAttributes.loaded === false) {
      setTimeout(() => {
        this.imageAttributes.loaded = true
      }, 100)
    }

    return this
  }
}
