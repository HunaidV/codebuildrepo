import { DEFAULT_FLOW, KBA_FLOW, MOBILE_ID_VERIFICATION_FLOW } from './flows'

export const START = 'start'
export const PAYMENT = 'payment'
export const PAYMENT_BY_PAYER = 'paymentByPayer'
export const PAYMENT_STATUS_WAIT = 'paymentStatusWait'
export const PERSONAL_DETAILS = 'personalDetails'
export const CHECK_PERSONAL_DETAILS = 'checkPersonalDetails'
export const ID_VERIFICATION_START = 'idVerificationStart'
export const ID_VERIFICATION_FRONT = 'idVerificationFront'
export const ID_VERIFICATION_FAILED = 'idVerificationFailed'
export const KBA_START = 'KBAStart'
export const KBA_IN_PROGRESS = 'KBAInProgress'
export const KBA_FAILS = 'KBAFails'
export const KBA_SUCCESS = 'KBAInSuccess'
export const SIGNATURE_INITIAL = 'signatureInitial'
export const SIGNATURE_START = 'signatureStart'
export const SIGNATURE_CAPTURED = 'signatureCaptured'
export const USER_IN_ROOM = 'userInRoom'
export const OFFICIAL_IN_ROOM = 'officialInRoom'
export const USER_SIGNING_PROCESS_START = 'userSigningProcessStart'
export const USER_SIGNED = 'userSigned'
export const OFFICIAL_SEALED = 'officialSealed'
export const WAITING_FOR_THE_DOCUMENT = 'waitingForTheDocument'
export const SESSION_ENDED = 'sessionEnded'
export const USER_SESSION_ENDED = 'userSessionEnded'
export const SESSION_CANCELLED = 'cancelled'
export const CAMERA_CHECK_START = 'cameraCheckStart'
export const MOBILE_VERIFICATION_ID_CAPTURE = 'mobileVerificationIdCapture'
export const MOBILE_VERIFICATION_SIGNATURE_INITIAL = 'mobileVerificationSignatureInitial'
export const MOBILE_VERIFICATION_SIGNATURE_CREATION = 'mobileVerificationSignatureCreation'
export const MOBILE_VERIFICATION_PERSONAL_DETAILS = 'mobileVerificationPersonalDetails'

export const INITIAL_SCREEN_RECORDING_STAGE = OFFICIAL_IN_ROOM

export const ORDERED = {
  [DEFAULT_FLOW]: [
    START,
    PERSONAL_DETAILS,
    SIGNATURE_INITIAL,
    SIGNATURE_START,
    SIGNATURE_CAPTURED,
    USER_IN_ROOM,
    OFFICIAL_IN_ROOM,
    USER_SIGNING_PROCESS_START,
    USER_SIGNED,
    OFFICIAL_SEALED,
    WAITING_FOR_THE_DOCUMENT,
    SESSION_ENDED,
    PAYMENT_BY_PAYER,
    PAYMENT,
    PAYMENT_STATUS_WAIT,
    USER_SESSION_ENDED
  ],
  [KBA_FLOW]: [
    START,
    ID_VERIFICATION_FRONT,
    MOBILE_VERIFICATION_ID_CAPTURE,
    MOBILE_VERIFICATION_PERSONAL_DETAILS,
    MOBILE_VERIFICATION_SIGNATURE_INITIAL,
    MOBILE_VERIFICATION_SIGNATURE_CREATION,
    PERSONAL_DETAILS,
    CAMERA_CHECK_START,
    SIGNATURE_INITIAL,
    SIGNATURE_START,
    SIGNATURE_CAPTURED,
    USER_IN_ROOM,
    OFFICIAL_IN_ROOM,
    CHECK_PERSONAL_DETAILS,
    KBA_START,
    KBA_IN_PROGRESS,
    KBA_SUCCESS,
    USER_SIGNING_PROCESS_START,
    USER_SIGNED,
    OFFICIAL_SEALED,
    WAITING_FOR_THE_DOCUMENT,
    SESSION_ENDED,
    PAYMENT_BY_PAYER,
    PAYMENT,
    PAYMENT_STATUS_WAIT,
    USER_SESSION_ENDED
  ],
  [MOBILE_ID_VERIFICATION_FLOW]: [
    MOBILE_VERIFICATION_ID_CAPTURE,
    MOBILE_VERIFICATION_PERSONAL_DETAILS,
    MOBILE_VERIFICATION_SIGNATURE_INITIAL,
    MOBILE_VERIFICATION_SIGNATURE_CREATION,
    CAMERA_CHECK_START
  ]
}

export const ORDERED_STAGE_INDEX_MAP = Object.keys(ORDERED).reduce((all, key) => ({
  ...all,
  [key]: ORDERED[key].reduce((map, curr, index) => ({
    ...map,
    [curr]: index
  }), {})
}), {})

export const getStageIndex = (stage, flow = DEFAULT_FLOW) => {
  if (ORDERED_STAGE_INDEX_MAP[flow][stage] === undefined) {
    return -1
  }

  return ORDERED_STAGE_INDEX_MAP[flow][stage]
}

export const getPrevStage = (curr, flow = DEFAULT_FLOW) => {
  const index = getStageIndex(curr, flow)

  if (index <= 1) {
    return ORDERED[flow][0]
  }

  return ORDERED[flow][index - 1]
}

export const getNextStage = (curr, flow = DEFAULT_FLOW) => {
  const index = getStageIndex(curr, flow)

  if (index === -1 || index >= ORDERED[flow].length - 1) {
    return ORDERED[flow][0]
  }

  return ORDERED[flow][index + 1]
}

export const getFirstStage = (flow = DEFAULT_FLOW) => ORDERED[flow][0]

export const isStageEq = (currStage, stage, flow = DEFAULT_FLOW) => {
  const neededStageIndex = getStageIndex(stage, flow)
  const currStageIndex = getStageIndex(currStage, flow)

  return neededStageIndex === currStageIndex
}

export const isStageBeforeOrEq = (currStage, stage, flow = DEFAULT_FLOW) => {
  const neededStageIndex = getStageIndex(stage, flow)
  const currStageIndex = getStageIndex(currStage, flow)

  return neededStageIndex <= currStageIndex
}

export const isStageBetweenOrEq = (currStage, start, end, flow = DEFAULT_FLOW) => {
  const startIndex = getStageIndex(start, flow)
  const endIndex = getStageIndex(end, flow)
  const currIndex = getStageIndex(currStage, flow)

  return currIndex >= startIndex && currIndex <= endIndex
}

export const isStageBefore = (currStage, stage, flow = DEFAULT_FLOW) => {
  const neededStageIndex = getStageIndex(stage, flow)
  const currStageIndex = getStageIndex(currStage, flow)

  return currStageIndex < neededStageIndex
}

export const isStageAfter = (currStage, stage, flow = DEFAULT_FLOW) => {
  const neededStageIndex = getStageIndex(stage, flow)
  const currStageIndex = getStageIndex(currStage, flow)

  return currStageIndex > neededStageIndex
}

export const isStageAfterOrEq = (currStage, stage, flow = DEFAULT_FLOW) => {
  const neededStageIndex = getStageIndex(stage, flow)
  const currStageIndex = getStageIndex(currStage, flow)

  return currStageIndex >= neededStageIndex
}
