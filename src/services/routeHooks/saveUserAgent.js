// @flow
import { Store } from 'vuex'
import { NotarySession } from '../NotarySession'
import type { AdditionalHookParameters } from '../../interfaces/Hooks'
import type { PrincipalDevice } from '../../interfaces/PrincipalDevice'
import { ROLE_MOBILE_VERIFICATION_USER, ROLE_USER } from '../../constants/roles'

// Saves device and browser info for principal
export default async (store: Store, { role }: AdditionalHookParameters): Promise<void> => {
  const isUser = role === ROLE_USER || role === ROLE_MOBILE_VERIFICATION_USER

  if (isUser === false) {
    return
  }

  const navigator: PrincipalDevice = ['platform', 'userAgent', 'appVersion', 'vendor']
    .reduce((all: PrincipalDevice, curr: string) => {
      if (window.navigator[curr]) {
        all[curr] = window.navigator[curr]
      }

      return all
    }, {})

  if (Object.keys(navigator).length > 0) {
    const session = NotarySession.init()

    await session.updateSession({ principalDevice: navigator })
  }
}
