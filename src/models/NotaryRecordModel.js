import Model from './Model'

import { typeOfNotarizationOptions, utils } from 'shared'

export const KBA_ID_EVIDENCE = 1
export const PERSONAL_KNOWLEDGE_EVIDENCE = 2

export class NotaryRecordModel extends Model {
  constructor () {
    super()

    this.id = null
    this.fee = '$25'
    this.notarizationDate = this.normalizeDate()
    this.signerName = null
    this.signerAddress = null
    this.signerCity = null
    this.idEvidence = 'Signer is personally known to the Online Notary Public'
    this.documentDate = null
    this.documentName = null
    this.typeOfNotarization = 1
    this.typeOfNotarizationOptions = utils.clone(typeOfNotarizationOptions)
    this.idEvidence = KBA_ID_EVIDENCE
    this.idEvidenceOptions = [
      { text: 'Identity proofing & credential analysis', value: KBA_ID_EVIDENCE },
      { text: 'Personal knowledge of principal', value: PERSONAL_KNOWLEDGE_EVIDENCE }
    ]
    this.documentId = null

    this.setFormElements([
      {
        label: 'Notarization date',
        type: 'datepicker',
        property: 'notarizationDate'
      }, {
        label: 'Signer Name',
        type: 'text',
        property: 'signerName'
      }, {
        label: 'Signer address',
        type: 'text',
        property: 'signerAddress'
      }, {
        label: 'Signer city',
        type: 'text',
        property: 'signerCity'
      }, {
        label: 'Document date',
        type: 'text',
        property: 'documentDate'
      }, {
        label: 'Document name/description',
        type: 'text',
        property: 'documentName'
      }, {
        label: 'Type of notarization',
        type: 'dropdown',
        property: 'typeOfNotarization',
        options: this.typeOfNotarizationOptions
      }, {
        label: 'Evidence of identity',
        type: 'dropdown',
        property: 'idEvidence',
        options: this.idEvidenceOptions
      }, {
        label: 'Fee',
        type: 'text',
        property: 'fee'
      }
    ])
  }

  setData (data) {
    this.fee = data.fee
    this.notarizationDate = this.normalizeDate(data.notarizationDate.toDate())
    this.signerName = data.signerName
    this.signerAddress = data.signerAddress
    this.signerCity = data.signerCity
    this.idEvidence = data.idEvidence
    this.documentDate = data.documentDate
    this.documentName = data.documentName
    this.typeOfNotarization = data.typeOfNotarization
    this.documentId = data.documentId || null

    for (let i = 0; i < this.idEvidenceOptions.length; i++) {
      if (this.idEvidenceOptions[i].text === data.idEvidence) {
        this.idEvidence = this.idEvidenceOptions[i].value
      }
    }

    return this
  }

  getData () {
    let typeOfNotarizationText = null
    let idEvidence = null

    for (let i = 0; i < this.typeOfNotarizationOptions.length; i++) {
      if (this.typeOfNotarizationOptions[i].value === this.typeOfNotarization) {
        typeOfNotarizationText = this.typeOfNotarizationOptions[i].text
      }
    }

    for (let i = 0; i < this.idEvidenceOptions.length; i++) {
      if (this.idEvidenceOptions[i].value === this.idEvidence) {
        idEvidence = this.idEvidenceOptions[i].text
      }
    }

    const data = {
      fee: this.fee,
      notarizationDate: new Date(this.notarizationDate),
      signerName: this.signerName,
      signerAddress: this.signerAddress,
      signerCity: this.signerCity,
      documentDate: this.documentDate,
      documentName: this.documentName,
      typeOfNotarization: this.typeOfNotarization,
      typeOfNotarizationText,
      idEvidence,
      documentId: this.documentId
    }

    if (this.status || this.finishedAt) {
      return {
        ...data,
        status: this.status,
        finishedAt: this.finishedAt
      }
    }

    return data
  }
}
