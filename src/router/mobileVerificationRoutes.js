import {
  IdFrontConfirmation, CreateSignature, Signature,
  PersonalDetails
} from './../views/user'
import {
  MOBILE_VERIFICATION_ID_CAPTURE, MOBILE_VERIFICATION_SIGNATURE_INITIAL, MOBILE_VERIFICATION_SIGNATURE_CREATION,
  MOBILE_VERIFICATION_PERSONAL_DETAILS, CAMERA_CHECK_START
} from './../constants/stages'
import { Finish } from './../views/mobileVerification'
import { ROLE_MOBILE_VERIFICATION_USER } from '../constants/roles'
import { MOBILE_VERIFICATION_BASE_ROUTE } from './../constants/routeParts'

export default [
  {
    path: `${MOBILE_VERIFICATION_BASE_ROUTE}/id`,
    name: 'mobileIdVerification',
    component: IdFrontConfirmation,
    meta: {
      allowedStages: {
        [ROLE_MOBILE_VERIFICATION_USER]: [MOBILE_VERIFICATION_ID_CAPTURE]
      }
    }
  }, {
    path: `${MOBILE_VERIFICATION_BASE_ROUTE}/signature`,
    name: 'mobileVerificationSignatureInitial',
    component: CreateSignature,
    meta: {
      allowedStages: {
        [ROLE_MOBILE_VERIFICATION_USER]: [MOBILE_VERIFICATION_SIGNATURE_INITIAL]
      }
    }
  }, {
    path: `${MOBILE_VERIFICATION_BASE_ROUTE}/signature/create`,
    name: 'mobileVerificationSignatureCreate',
    component: Signature,
    meta: {
      allowedStages: {
        [ROLE_MOBILE_VERIFICATION_USER]: [MOBILE_VERIFICATION_SIGNATURE_CREATION]
      }
    }
  }, {
    path: `${MOBILE_VERIFICATION_BASE_ROUTE}/personal-details`,
    name: 'mobileVerificationPersonalDetails',
    component: PersonalDetails,
    meta: {
      allowedStages: {
        [ROLE_MOBILE_VERIFICATION_USER]: [MOBILE_VERIFICATION_PERSONAL_DETAILS]
      }
    }
  }, {
    path: `${MOBILE_VERIFICATION_BASE_ROUTE}/finish`,
    name: 'mobileVerificationFinish',
    component: Finish,
    meta: {
      allowedStages: {
        [ROLE_MOBILE_VERIFICATION_USER]: [CAMERA_CHECK_START]
      }
    }
  }
]
