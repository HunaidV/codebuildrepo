import BasicStorage from './../../BasicStorage'

export default class KBALocalStorage extends BasicStorage {
  constructor () {
    super(window.localStorage)
    this._storageKeys = {
      answers: 'kbaAnswers',
      complete: 'kba'
    }
  }

  getAnswers () {
    return this._get(this._storageKeys.answers, {})
  }

  setAnswers (answers) {
    this._set(this._storageKeys.answers, answers)

    return this
  }

  removeAnswers () {
    this._remove(this._storageKeys.answers)

    return this
  }

  setCompleteData (data) {
    this._set(this._storageKeys.complete, data)

    return this
  }

  getCompleteData () {
    return this._get(this._storageKeys.complete)
  }

  getQuestions () {
    const kba = this._get(this._storageKeys.complete)

    if (kba) {
      const { output: { questions: { questions } } } = kba

      return questions
    }

    return null
  }
}
