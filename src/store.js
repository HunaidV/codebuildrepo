// @flow
import Vue from 'vue'
import Vuex from 'vuex'
import RouteGuard from './services/RouteGuard/RouteGuard'
import { NO_SUB_STAGE } from './constants/subStages'

Vue.use(Vuex)

export const build = () => (
  new Promise<Object>((resolve, reject) => {
    const storeInstance = new Vuex.Store({
      state: {
        stage: null,
        officialUid: null,
        user: {},
        userInfo: {},
        startRecording: null,
        stopRecording: null,
        sessionId: null,
        retakes: 0,
        role: null,
        isRecorderHidden: true,
        recorderParentNode: null,
        isRecordingInProgress: false,
        isRecordUploaded: true,
        allRulesSelected: false,
        idParts: null,
        isIdVisible: false,
        isSupportWindowVisible: false,
        events: [],
        idRetakeCounter: 0,
        signatureImage: null,
        typeOfNotarization: null,
        isIdPopupVisible: false,
        subStage: NO_SUB_STAGE,
        principalDevice: null,
        signatureBoxPointers: [],
        commands: [],
        requestId: null
      },
      mutations: {
        changeState (state: Object, data: Object) {
          const props = Object.keys(data)

          for (let i = 0; i < props.length; i++) {
            state[props[i]] = data[props[i]]
          }
        },
        toStage (state: Object, stage: string) {
          state.stage = stage
        },
        changeRole (state: Object, role: string) {
          state.role = role
        },
        startRecording (state: Object) {
          state.startRecording = true
          state.stopRecording = false
        },
        stopRecording (state: Object) {
          state.startRecording = false
          state.stopRecording = true
        },
        setRecordingIsInProgress (state: Object) {
          state.isRecordingInProgress = true
        },
        setRecordingIsNotInProgress (state: Object) {
          state.isRecordingInProgress = false
        },
        hideRecorder (state: Object) {
          state.isRecorderHidden = true
        },
        showRecorder (state: Object) {
          state.isRecorderHidden = false
        },
        moveRecorderWindowTo (state: Object, id: string) {
          state.recorderParentNode = id
        },
        setRecordUploaded (state: Object) {
          state.isRecordUploaded = true
        },
        setAllRulesSelected (state: Object) {
          state.allRulesSelected = true
        },
        showIdPreviewer (state: Object) {
          state.isIdVisible = true
        },
        hideIdPreviewer (state: Object) {
          state.isIdVisible = false
        },
        showSupportWindow (state: Object) {
          state.isSupportWindowVisible = true
        },
        hideSupportWindow (state: Object) {
          state.isSupportWindowVisible = false
        },
        showIdPopupPreviewer (state: Object) {
          state.isIdPopupVisible = true
        },
        hideIdPopupPreviewer (state: Object) {
          state.isIdPopupVisible = false
        },
        setSubStage (state: Object, subStage: string) {
          state.subStage = subStage
        }
      },
      plugins: [async store => {
        try {
          await RouteGuard.createStorePlugin(store)
          resolve(storeInstance)
        } catch (e) {
          reject(e)
        }
      }]
    })
  })
)
