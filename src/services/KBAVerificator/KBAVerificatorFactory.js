import VeratadKBAVerificator from './verificators/VeratadKBAVerificator'
import KBALocalStorage from './storages/KBALocalStorage'

export default class KBAVerificatorFactory {
  static create () {
    return new VeratadKBAVerificator({ storage: new KBALocalStorage() })
  }
}
