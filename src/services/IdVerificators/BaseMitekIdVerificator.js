import axios from 'axios'
import { loadScript } from '../utils'
import { ID_VERIFICATION_ERRORS } from '../../constants/messages'

export default class BaseMitekIdVerificator {
  constructor ({ logger, name }) {
    this.apiBaseUrl = process.env.VUE_APP_API_BASE_URL
    this.name = name
    this.logger = logger
    this.src = {
      initial: [],
      dependent: []
    }
  }

  setCredentials (credentials) {
    this.credentials = credentials

    return this
  }

  async _loadScripts () {
    try {
      const promises = []

      for (let i = 0; i < this.src.initial.length; i++) {
        promises.push(loadScript(this.src.initial[i]))
      }
      for (let i = 0; this.src.dependent && i < this.src.dependent.length; i++) {
        promises.push(loadScript(this.src.dependent[i]))
      }
      await Promise.all(promises)
    } catch (e) {
      if (process.env.VUE_APP_IGNORE_WEB_ASSEMBLY_ERROR) {
        return
      }

      throw e
    }
  }

  async verify (image) {
    const requestUrl = `${this.apiBaseUrl}/id/verify`
    const params = { headers: this.credentials }
    const { data } = await axios.post(requestUrl, { image }, params)

    return data
  }

  async getFrontImageUrl ({ filename }) {
    const requestUrl = `${this.apiBaseUrl}/id/url?filename=${filename}`
    const params = { headers: this.credentials }
    const { data: { url } } = await axios.get(requestUrl, params)

    return url
  }

  translateErrors (data) {
    const result = []

    if (!data) {
      return []
    }

    const codes = Object.keys(data)

    for (let i = 0; i < codes.length; i++) {
      let currError = data[codes[i]]

      if (ID_VERIFICATION_ERRORS[codes[i]]) {
        currError = ID_VERIFICATION_ERRORS[codes[i]]
      }

      result.push(currError)
    }

    return result
  }
}
