// @flow
import type { PrincipalDevice } from '../interfaces/PrincipalDevice'

interface DeviceData {
  name: string,
  value: string,
  version: string
}

class UserAgentService {
  +dataOS: Array<DeviceData> = [
    { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
    { name: 'Windows', value: 'Win', version: 'NT' },
    { name: 'iPhone', value: 'iPhone', version: 'OS' },
    { name: 'iPad', value: 'iPad', version: 'OS' },
    { name: 'Kindle', value: 'Silk', version: 'Silk' },
    { name: 'Android', value: 'Android', version: 'Android' },
    { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
    { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
    { name: 'Macintosh', value: 'Mac', version: 'OS X' },
    { name: 'Linux', value: 'Linux', version: 'rv' },
    { name: 'Palm', value: 'Palm', version: 'PalmOS' }
  ];
  +dataBrowser: Array<DeviceData> = [
    { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
    { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
    { name: 'Safari', value: 'Safari', version: 'Version' },
    { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
    { name: 'Opera', value: 'Opera', version: 'Opera' },
    { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
    { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
  ]

  match (string: string, data: Array<DeviceData>): ?string {
    let regex
    let regexv
    let match
    let matches
    let version

    for (let i = 0; i < data.length; i += 1) {
      regex = new RegExp(data[i].value, 'i')
      match = regex.test(string)

      if (match) {
        regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i')
        matches = string.match(regexv)
        version = ''

        if (matches && matches[1]) {
          matches = matches[1]
        }

        if (matches) {
          // $FlowFixMe
          matches = matches.split(/[._]+/)

          for (let j = 0; j < matches.length; j += 1) {
            if (j === 0) {
              version += matches[j] + '.'
            } else {
              version += matches[j]
            }
          }
        } else {
          version = '0'
        }

        return [data[i].name, parseFloat(version)]
          .filter((o: ?string|number): boolean => {
            if (typeof o === 'string' && o) {
              return true
            }

            return version !== null && version !== undefined && !isNaN(version)
          }).join(' ')
      }
    }

    return null
  }

  decodeOS (navigator: PrincipalDevice): ?string {
    const str = Object.values(navigator).join(' ')

    return this.match(str, this.dataOS)
  }

  decodeBrowser (navigator: PrincipalDevice): ?string {
    const str = Object.values(navigator).join(' ')

    return this.match(str, this.dataBrowser)
  }
}

export default new UserAgentService()
