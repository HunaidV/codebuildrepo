// @flow
import GraphicNode from './GraphicNode'
import type { Deferred } from '../../services/utils'
import type { Rectangle } from '../../interfaces/Rectangle'
import { toInt, defer, createImageUrl } from '../../services/utils'
import type { PositionedRectangle } from '../../interfaces/PositionedRectangle'

export default class SignatureImageNode extends GraphicNode {
  _parentDocument: Rectangle|null = null
  _parentDocumentSetPromise: Deferred = defer()
  _originalImageData: string|null = null
  _imageData: string|null = null
  _startMaxWidth: number = 150
  _startMaxHeight: number = 150
  _scaleStep: number = 10

  isSignatureLoaded: boolean = false
  height: number = 1
  width: number
  minHeight: number = 20
  stickSize: number = 10
  x: number = 0
  y: number = 0

  constructor () {
    super()
    this.width = this._startMaxWidth
  }

  setImageData (imageData: string|null): this {
    if (imageData) {
      this._originalImageData = imageData
      this._imageData = createImageUrl(imageData)
    }

    return this
  }

  getImageData (): string|null {
    return this._imageData
  }

  getOriginalImageData (): string|null {
    return this._originalImageData
  }

  async setParameters ({ width, height }: Rectangle): Promise<Rectangle> {
    let newWidth = width
    let newHeight = height

    if (width > this._startMaxWidth || height > this._startMaxHeight) {
      const heightRation = this._startMaxHeight / height
      const widthRation = this._startMaxWidth / width
      let ratio = heightRation

      if (heightRation > widthRation) {
        ratio = widthRation
      }

      newWidth = toInt(width * ratio)
      newHeight = toInt(height * ratio)
    }

    this.height = newHeight
    this.width = newWidth

    await this._parentDocumentSetPromise

    this.isSignatureLoaded = true

    return {
      height: this.height,
      width: this.width
    }
  }

  setParentDocument (document: Rectangle): this {
    this._parentDocument = document
    this.x = this._parentDocument.width / 2 - 100
    this.y = this._parentDocument.height / 2 - 20
    this._parentDocumentSetPromise.resolve()

    return this
  }

  scaleUp (): this {
    return this.scaleBy(this._scaleStep)
  }

  scaleDown (): this {
    return this.scaleBy(-this._scaleStep)
  }

  scaleBy (step: number): this {
    const newHeight = this.height + step
    const ratio = newHeight / this.height

    if (newHeight >= this.minHeight) {
      this.width = toInt(this.width * ratio)
      this.height = toInt(newHeight)
    }

    return this
  }

  placeInBox ({ top, left, width, height }: PositionedRectangle): this {
    this.y = top
    this.x = left
    this.width = width
    this.height = height

    return this
  }

  get startXPosition (): number {
    return this.x
  }

  get startYPosition (): number {
    return this.y
  }
}
