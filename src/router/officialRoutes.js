import {
  START,
  PAYMENT,
  PAYMENT_BY_PAYER,
  PAYMENT_STATUS_WAIT,
  ID_VERIFICATION_START,
  ID_VERIFICATION_FRONT,
  ID_VERIFICATION_FAILED,
  KBA_START,
  KBA_IN_PROGRESS,
  KBA_SUCCESS,
  SIGNATURE_INITIAL,
  SIGNATURE_START,
  SIGNATURE_CAPTURED,
  USER_IN_ROOM,
  OFFICIAL_IN_ROOM,
  USER_SIGNING_PROCESS_START,
  USER_SIGNED,
  OFFICIAL_SEALED,
  SESSION_ENDED,
  PERSONAL_DETAILS,
  SESSION_CANCELLED,
  CAMERA_CHECK_START,
  KBA_FAILS,
  MOBILE_VERIFICATION_ID_CAPTURE,
  MOBILE_VERIFICATION_SIGNATURE_INITIAL,
  MOBILE_VERIFICATION_SIGNATURE_CREATION,
  MOBILE_VERIFICATION_PERSONAL_DETAILS,
  WAITING_FOR_THE_DOCUMENT,
  CHECK_PERSONAL_DETAILS,
  USER_SESSION_ENDED
} from './../constants/stages'
import {
  OfficialSealing, OfficialInitial, OfficialComplete,
  OfficialReview, Cancelled
} from './../views/official'

export default [
  {
    path: '/official',
    name: 'officialInitial',
    component: OfficialInitial,
    meta: {
      allowedOfficialStages: [
        START,
        PERSONAL_DETAILS,
        CAMERA_CHECK_START,
        KBA_START,
        KBA_IN_PROGRESS,
        KBA_SUCCESS,
        ID_VERIFICATION_START,
        ID_VERIFICATION_FRONT,
        ID_VERIFICATION_FAILED,
        SIGNATURE_INITIAL,
        SIGNATURE_START,
        SIGNATURE_CAPTURED,
        USER_IN_ROOM,
        OFFICIAL_IN_ROOM,
        KBA_FAILS,
        MOBILE_VERIFICATION_ID_CAPTURE,
        MOBILE_VERIFICATION_PERSONAL_DETAILS,
        MOBILE_VERIFICATION_SIGNATURE_INITIAL,
        MOBILE_VERIFICATION_SIGNATURE_CREATION,
        CHECK_PERSONAL_DETAILS
      ]
    }
  }, {
    path: '/official/sealing',
    name: 'officialSealing',
    component: OfficialSealing,
    meta: {
      allowedOfficialStages: [
        USER_SIGNING_PROCESS_START,
        USER_SIGNED,
        WAITING_FOR_THE_DOCUMENT
      ]
    }
  }, {
    path: '/official/complete',
    name: 'officialComplete',
    component: OfficialComplete,
    meta: {
      allowedOfficialStages: [OFFICIAL_SEALED]
    }
  }, {
    path: '/official/review',
    name: 'officialReview',
    component: OfficialReview,
    meta: {
      allowedOfficialStages: [
        SESSION_ENDED,
        PAYMENT,
        PAYMENT_BY_PAYER,
        PAYMENT_STATUS_WAIT,
        USER_SESSION_ENDED
      ]
    }
  }, {
    path: '/official/cancelled',
    name: 'officialCancelled',
    component: Cancelled,
    meta: {
      allowedOfficialStages: [
        SESSION_CANCELLED
      ]
    }
  }
]
