import BaseMitekIdVerificator from './BaseMitekIdVerificator'
import { defer, getBase64FromDataUrl, normalizeUrl } from '../utils'
import { ID_VERIFICATION_SDK_FAILURE } from '../../constants/events'
import {
  AUTO_CAPTURE_MODE, SHOW_HINT_COMMAND, HIDE_HINT_COMMAND,
  SDK_STOP_COMMAND, CAPTURE_AND_PROCESS_FRAME_COMMAND, CAMERA_DISPLAY_STARTED_EVENT,
  FRAME_PROCESSING_STARTED_EVENT, FRAME_CAPTURE_RESULT_EVENT, SDK_ERROR_EVENT,
  FRAME_PROCESSING_FEEDBACK_EVENT, MITEK_ERROR_GLARE_ERROR
} from './../../constants/mitek'

export default class MitekIdVerificator extends BaseMitekIdVerificator {
  constructor (data) {
    super(data)
    this.timer = null
    this.glareTimer = null
    this.glareCounter = 0
    this.autoCaptureTimeout = 20000
    this.mitekPath = normalizeUrl(`${process.env.BASE_URL}/js/mitek_4.2`)
    this.mitekSDKPath = `${this.mitekPath}/mitekSDK`
    this.src = {
      initial: [
        `${this.mitekPath}/mitek-science-sdk.js`
      ]
    }
    this.qualityPercent = 50
    this.captureMode = AUTO_CAPTURE_MODE
  }

  async loadSrc () {
    if (!window.mitekScienceSDK) {
      this.logger.debug('Mitek 4.2 initial load')
      await this._loadScripts()
    }

    this.mitekScienceSDK = window.mitekScienceSDK

    return this
  }

  startAutoCapture (docType, { hint } = {}) {
    const capturedPromise = defer()

    this.mitekScienceSDK.on(CAMERA_DISPLAY_STARTED_EVENT, () => {
      if (hint) {
        this.mitekScienceSDK.cmd(SHOW_HINT_COMMAND, hint)
      }
      this.glareTimer = setTimeout(() => this.mitekScienceSDK.cmd(HIDE_HINT_COMMAND), this.autoCaptureTimeout)
    })
    this.mitekScienceSDK.on(FRAME_PROCESSING_STARTED_EVENT, () => {
      this.timer = setTimeout(() => {
        capturedPromise.reject('Timeout')
        this.stopAutoCapture()
      }, this.autoCaptureTimeout)
    })
    this.mitekScienceSDK.on(FRAME_CAPTURE_RESULT_EVENT, (result) => {
      clearTimeout(this.timer)

      return capturedPromise.resolve({
        image: getBase64FromDataUrl(result.response.imageData),
        data: result
      })
    })
    this.mitekScienceSDK.on(SDK_ERROR_EVENT, e => (
      capturedPromise.reject({ error: e, event: ID_VERIFICATION_SDK_FAILURE })
    ))
    this.mitekScienceSDK.on(FRAME_PROCESSING_FEEDBACK_EVENT, (result) => {
      if (result.frameError === MITEK_ERROR_GLARE_ERROR) {
        this.glareCounter++

        if (this.glareCounter > 2) {
          this.glareCounter = 0
          this.mitekScienceSDK.cmd(SHOW_HINT_COMMAND, 'Reduce Glare')
          clearTimeout(this.glareTimer)
          this.glareTimer = setTimeout(() => this.mitekScienceSDK.cmd(HIDE_HINT_COMMAND), 5000)
        }
      }
    })
    this.mitekScienceSDK.cmd(CAPTURE_AND_PROCESS_FRAME_COMMAND, {
      mode: this._getCaptureMode(),
      documentType: docType,
      mitekSDKPath: this.mitekSDKPath,
      options: {
        qualityPercent: this.qualityPercent
      }
    })
    window.scrollTo(0, 0)

    return capturedPromise.promise
  }

  stopAutoCapture () {
    this.mitekScienceSDK.cmd(SDK_STOP_COMMAND)
  }

  _getCaptureMode () {
    return AUTO_CAPTURE_MODE
  }
}
