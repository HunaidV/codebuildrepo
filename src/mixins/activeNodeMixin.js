// @flow
import { USER_SIGNING_PROCESS_START } from '../constants/stages'

let activeNodeOngoingProcess = null

export default {
  data: () => ({
    activeNode: null
  }),
  watch: {
    stage (stage: string): void {
      if (stage !== USER_SIGNING_PROCESS_START) {
        this.toggleActiveNode(null, false)
      }
    }
  },
  methods: {
    toggleActiveNode (node: ?Node, status: boolean): void {
      if (activeNodeOngoingProcess) {
        clearTimeout(activeNodeOngoingProcess)
      }

      if (status) {
        this.activeNode = node

        return
      }

      activeNodeOngoingProcess = setTimeout(() => {
        this.activeNode = null

        return false
      }, 200)
    }
  }
}
