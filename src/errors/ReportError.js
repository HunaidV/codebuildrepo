// @flow

export default class ReportError extends Error {
  extra: any
}
