import MitekIdVerificator from './MitekIdVerificator'
import { MANUAL_CAPTURE_MODE } from '../../constants/mitek'

export default class MitekLegacyIdVerificator extends MitekIdVerificator {
  _getCaptureMode () {
    return MANUAL_CAPTURE_MODE
  }
}
