// @flow
import { Store } from 'vuex'

export interface AdditionalHookParameters {
  role?: string
}

export type Hook = (store: Store, additionalHookParameters?: AdditionalHookParameters) => void
export type AsyncHook = (store: Store, additionalHookParameters?: AdditionalHookParameters) => Promise<void>
