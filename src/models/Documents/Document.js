import { STATUS_NOTARIZED } from '../../constants/documents'
import { sortBy } from '../../services/utils'

export default class Document {
  constructor (db) {
    this.db = db
  }

  static create (db) {
    return new Document(db)
  }

  async getAllBySessionId (sessionId) {
    const snapshot = await this.db.collection('notary-documents')
      .where('sessionId', '==', sessionId)
      .get()
    const docs = snapshot.docs.map(doc => ({
      ...doc.data(),
      id: doc.id
    }))

    return sortBy(docs, 'name')
  }

  storeSignedKey (id, key) {
    return this.db.collection('notary-documents')
      .doc(id)
      .update({ keySigned: key })
  }

  setNotarized (id, keySealed) {
    return this.db.collection('notary-documents')
      .doc(id)
      .update({ status: STATUS_NOTARIZED, keySealed })
  }
}
