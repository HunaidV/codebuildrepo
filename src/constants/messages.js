export const CAPTURE_FRONT_ID_MESSAGE = 'Place your driver\'s license on a flat surface, with its front-side facing up. ' +
  'Then position it inside this frame.'
export const CAPTURE_BACK_ID_MESSAGE = 'Position barcode on the back of driver\'s license inside the frame.'
export const ID_VERIFICATION_ERRORS = {
  1: 'A barcode symbol was not found',
  2: 'Out of focus',
  3: 'Image too dark / bad lighting',
  4: 'Below minimum fill / camera too distant',
  5: 'Image skewed',
  6: 'Barcode symbol too far away',
  7: 'Glare detected',
  8: 'Four corners not detected',
  9: 'Corrupt image',
  10: 'Not enough padding / too close',
  11: 'Low contrast',
  12: 'Busy background',
  13: 'Image too small / < 450 pixels'
}
