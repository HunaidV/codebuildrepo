import Logger from '../../services/Logger'
import MitekIdVerificator from './MitekIdVerificator'
import MitekLegacyIdVerificator from './MitekLegacyIdVerificator'
import { LEGACY, AUTO_CAPTURE } from '../../constants/idVerificators'

let priority = {
  [LEGACY]: MitekLegacyIdVerificator,
  [AUTO_CAPTURE]: MitekIdVerificator
}

export default class IdVerificatorFactory {
  static create (name) {
    if (!priority[name]) {
      throw new Error(`No ${name} Id Verificator defined`)
    }

    return new priority[name]({
      name,
      logger: new Logger()
    })
  }
}
