// @flow
import GraphicNode from './GraphicNode'
import { getTextSize, toInt } from './../../services/utils'

const standardFontSize = 12
const minWidth = 70
const offset = 5

export default class TextNode extends GraphicNode {
  number: number
  textId: string
  text: string
  fontSize: number
  stickSize: number
  width: number
  height: number
  isScalable: boolean
  documentWidth: number
  y: number

  constructor ({ id, number }: { id: string, number: number }) {
    super()
    this.number = number
    this.id = `text-node-container-${id}`
    this.textId = `text-node-${id}`
    this.text = 'Text'
    this.isEditable = true
    this.fontSize = standardFontSize
    this.stickSize = 8
    this.width = minWidth
    this.height = 50
    this.isScalable = true
  }

  setText (text: string): this {
    this.text = text

    return this
  }

  setDocumentWidth (documentWidth: number): this {
    this.documentWidth = documentWidth

    return this
  }

  setStartPosition ({ y }: { y: number }): this {
    const { top } = this.getOffset()

    this.y = toInt(y) + top

    return this
  }

  getEndPosition (): { y: number } {
    return {
      y: this.y + this.height
    }
  }

  getOffset (): { top: number, bottom: number } {
    return {
      top: 5,
      bottom: 0
    }
  }

  setWidth (w: number): this {
    this.width = w

    return this
  }

  calculate (): this {
    this._scaleSize()

    return this
  }

  get x (): number {
    return toInt(this.documentWidth * 2 / 3)
  }

  get fontSizePx (): string {
    return `${this.fontSize}px`
  }

  _scaleSize (): this {
    let width: number = 0
    let height: number = 0

    const maxWidth = this.documentWidth * 2 / 3
    const size = getTextSize(this.text, this.fontSize, maxWidth)

    if (size) {
      width = size.width + offset
      height = size.height + offset
    }

    if (width > maxWidth) {
      width = maxWidth
    } else if (width < minWidth) {
      width = minWidth
    }

    this.width = width
    this.height = height

    return this
  }
}
