import axios from 'axios'
import { isFunction, makeUUID } from '../services/utils'
import { NotarySession } from '../services/NotarySession'

export const sendLogs = (data) => {
  if (isFunction(navigator.sendBeacon) === false) {
    return
  }

  return navigator.sendBeacon(`${process.env.VUE_APP_API_BASE_URL}/logs`, JSON.stringify({
    ...data,
    extra: data.extra && JSON.stringify(data.extra)
  }))
}

// On request
axios.interceptors.request.use((config) => {
  const requestId = makeUUID()

  sendLogs({
    sessionId: NotarySession.init().getSessionId(),
    component: 'HttpRequest',
    message: 'Request logging',
    level: 'debug',
    service: 'APP',
    userAgent: window.navigator.userAgent,
    extra: {
      requestId,
      currentUrl: window.location.href,
      requestToUrl: config.url,
      headers: config.headers.common,
      method: config.method
    }
  })

  config.requestId = requestId

  return config
}, (error) => {
  sendLogs({
    sessionId: NotarySession.init().getSessionId(),
    component: 'HttpRequest',
    message: 'Request logging',
    level: 'error',
    service: 'APP',
    userAgent: window.navigator.userAgent,
    extra: {
      requestId: error.config.requestId,
      currentUrl: window.location.href,
      requestToUrl: error.config.url,
      message: error.message
    }
  }).catch(() => {
    // Couldn't test, ignore errors for now
  })

  return Promise.reject(error)
})

// On response
axios.interceptors.response.use((response) => {
  sendLogs({
    sessionId: NotarySession.init().getSessionId(),
    component: 'HttpRequest',
    message: 'Request logging',
    level: 'debug',
    service: 'APP',
    userAgent: window.navigator.userAgent,
    extra: {
      requestId: response.config.requestId,
      currentUrl: window.location.href,
      status: response.status,
      headers: response.headers,
      data: response.data,
      url: response.config.url,
      method: response.config.method
    }
  })

  return response
}, (error) => {
  sendLogs({
    sessionId: NotarySession.init().getSessionId(),
    component: 'HttpRequest',
    message: 'Request logging',
    level: 'error',
    service: 'APP',
    userAgent: window.navigator.userAgent,
    extra: {
      requestId: error.config.requestId,
      currentUrl: window.location.href,
      status: error.response.status,
      headers: error.response.headers,
      data: error.response.data,
      url: error.config.url,
      method: error.config.method
    }
  })

  return Promise.reject(error)
})
