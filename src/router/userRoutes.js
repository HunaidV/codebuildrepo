import {
  START,
  PAYMENT,
  PAYMENT_STATUS_WAIT,
  ID_VERIFICATION_START,
  ID_VERIFICATION_FRONT,
  KBA_START,
  KBA_IN_PROGRESS,
  KBA_SUCCESS,
  SIGNATURE_INITIAL,
  SIGNATURE_START,
  SIGNATURE_CAPTURED,
  USER_IN_ROOM,
  OFFICIAL_IN_ROOM,
  USER_SIGNING_PROCESS_START,
  USER_SIGNED,
  OFFICIAL_SEALED,
  SESSION_ENDED,
  USER_SESSION_ENDED,
  PERSONAL_DETAILS,
  SESSION_CANCELLED,
  CAMERA_CHECK_START,
  KBA_FAILS,
  ID_VERIFICATION_FAILED,
  MOBILE_VERIFICATION_ID_CAPTURE,
  MOBILE_VERIFICATION_SIGNATURE_INITIAL,
  MOBILE_VERIFICATION_SIGNATURE_CREATION,
  MOBILE_VERIFICATION_PERSONAL_DETAILS,
  WAITING_FOR_THE_DOCUMENT,
  CHECK_PERSONAL_DETAILS,
  PAYMENT_BY_PAYER
} from '../constants/stages'
import {
  Start, Payment, PaymentByPayer, PaymentStatusWait, ConfInit, Signature,
  Sign, IdFrontConfirmation,
  KBAStart, KBAQuestions, CreateSignature,
  MeetingInit, PersonalDetails, Finish,
  Cancelled, CameraCheck, KBAFailed,
  KBASuccess, IdVerificationFailed, MobileVerificationProgress,
  DocumentWait, Waiting
} from '../views/user'

export default [
  {
    path: '/',
    name: 'start',
    component: Start,
    meta: {
      allowedStages: [START]
    }
  }, {
    path: '/loading',
    name: 'loading',
    component: Waiting,
    meta: {
      allowedStages: [SESSION_ENDED]
    }
  }, {
    path: '/payment',
    name: 'payment',
    component: Payment,
    meta: {
      allowedStages: [PAYMENT]
    }
  }, {
    path: '/payment-by-payer',
    name: 'paymentByPayer',
    component: PaymentByPayer,
    meta: {
      allowedStages: [PAYMENT_BY_PAYER]
    }
  }, {
    path: '/payment-status-wait',
    name: 'paymentStatusWait',
    component: PaymentStatusWait,
    meta: {
      allowedStages: [PAYMENT_STATUS_WAIT]
    }
  }, {
    path: '/camera',
    name: 'cameraCheck',
    component: CameraCheck,
    meta: {
      allowedStages: [CAMERA_CHECK_START]
    }
  }, {
    path: '/personal',
    name: 'personal',
    component: PersonalDetails,
    meta: {
      allowedStages: [PERSONAL_DETAILS]
    }
  }, {
    path: '/id-front',
    name: 'idFront',
    component: IdFrontConfirmation,
    meta: {
      allowedStages: [
        ID_VERIFICATION_START,
        ID_VERIFICATION_FRONT
      ]
    }
  }, {
    path: '/id-verification-failed',
    name: 'idVerificationFailed',
    component: IdVerificationFailed,
    meta: {
      allowedStages: [ID_VERIFICATION_FAILED]
    }
  }, {
    path: '/id-verification',
    name: 'kbaStart',
    component: KBAStart,
    meta: {
      allowedStages: [KBA_START]
    }
  }, {
    path: '/questions',
    name: 'questions',
    component: KBAQuestions,
    meta: {
      allowedStages: [KBA_IN_PROGRESS]
    }
  }, {
    path: '/kba-success',
    name: 'kbaSuccess',
    component: KBASuccess,
    meta: {
      allowedStages: [KBA_SUCCESS]
    }
  }, {
    path: '/kba-fail',
    name: 'kbaFailed',
    component: KBAFailed,
    meta: {
      allowedStages: [KBA_FAILS]
    }
  }, {
    path: '/signature-create',
    name: 'signatureCreate',
    component: CreateSignature,
    meta: {
      allowedStages: [SIGNATURE_INITIAL]
    }
  }, {
    path: '/signature',
    name: 'signature',
    component: Signature,
    meta: {
      allowedStages: [SIGNATURE_START]
    }
  }, {
    path: '/meeting/init',
    name: 'meetingInit',
    component: MeetingInit,
    meta: {
      allowedStages: [SIGNATURE_CAPTURED]
    }
  }, {
    path: '/meeting',
    name: 'meeting',
    component: ConfInit,
    meta: {
      allowedStages: [
        USER_IN_ROOM,
        OFFICIAL_IN_ROOM
      ]
    }
  }, {
    path: '/sign',
    name: 'sign',
    component: Sign,
    meta: {
      allowedStages: [
        USER_SIGNING_PROCESS_START,
        USER_SIGNED,
        OFFICIAL_SEALED
      ]
    }
  }, {
    path: '/wait',
    name: 'wait',
    component: DocumentWait,
    meta: {
      allowedStages: [WAITING_FOR_THE_DOCUMENT]
    }
  }, {
    path: '/finish',
    name: 'finish',
    component: Finish,
    meta: {
      allowedStages: [USER_SESSION_ENDED]
    }
  }, {
    path: '/cancelled',
    name: 'cancelled',
    component: Cancelled,
    meta: {
      allowedStages: [SESSION_CANCELLED]
    }
  }, {
    path: '/mobile-verification-progress',
    name: 'mobileVerificationProgress',
    component: MobileVerificationProgress,
    meta: {
      allowedStages: [
        MOBILE_VERIFICATION_ID_CAPTURE,
        MOBILE_VERIFICATION_PERSONAL_DETAILS,
        MOBILE_VERIFICATION_SIGNATURE_INITIAL,
        MOBILE_VERIFICATION_SIGNATURE_CREATION
      ]
    }
  }, {
    path: '/check-personal-details',
    name: 'checkPersonalDetails',
    component: PersonalDetails,
    meta: {
      allowedStages: [CHECK_PERSONAL_DETAILS]
    }
  }
]
