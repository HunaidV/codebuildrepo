// @flow
export interface ConvertedImages {
  signed: string;
  sealed: string;
  original: string;
}

export interface NotaryDocument {
  convertedImages: ConvertedImages;
  status?: string;
}
